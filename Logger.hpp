/**
 * @file Logger.hpp
 * Contains definition of class Logger.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include <fstream>
#include <iostream>
#include <pthread.h>

/**
 * Logs messages to a log file, standard error and optionally standard output.
 * Implemented as a singletor, call getInstance() to retrieve the shared instance.
 * Call destroy() to perform cleanup prior to terminating the program.
 * @see Logger::getInstance()
 * @see Logger::destroy()
 */
class Logger
{
public:
	static const int ERROR = 0; /**< Error - notify to log and stderr */
	static const int WARNING = 1; /**< Warning - notify to log */
	static const int INFO0 = 2; /**< Important information */
	static const int INFO1 = 3; /**< Interesting information */
	static const int INFO2 = 4; /**< Debug-level information */
private:
	static Logger *instance; /**< The singleton instance */

	std::ofstream file; /**< Log file stream */
	bool mutexed; /**< Use mutexes */
	int verbosity; /**< The level of verbosity */

	pthread_mutex_t mutex; /**< Logging mutex */
	bool opened; /**< Log file opened */
public:
	/**
	 * Returns a shared instance of Logger.
	 * @return Instance of Logger or NULL if log file is not set
	 */
	static Logger* getInstance();
	/**
	 * Closes the log file and performs cleanup.
	 */
	static void destroy();

	/**
	 * Opens a file to log to.
	 * Messages will be appended if the file already exists.
	 * Fails if the file cannot be opened for writing.
	 * @param[in] fileName File name/path of the log file
	 * @param[in] verbosity Level of verbosity (Logger::ERROR, Logger::WARNING, Logger::INFO0, Logger::INFO1, Logger::INFO2)
	 * @param[in] mutexed Mutex I/O calls
	 * @return Success
	 */
	static bool openFile(const std::string &fileName, int verbosity, bool mutexed);
	/**
	 * @copydoc Logger::openFile
	 */
	static bool openFile(const char *fileName, int verbosity, bool mutexed);
	/**
	 * Appens a message to the log file.
	 * @param[in] message Message
	 * @param[in] severity Serverity of the event
	 * @param[in] source Location we are logging from
	 */
	void log(const std::string &message, int severity, const std::string &source = "");
private:
	/**
	 * Hidden for singleton.
	 * Does basic initialization.
	 * @see Logger::getInstance()
	 */
	Logger();
	/**
	 * Hidden for singleton.
	 * Closes the log file stream if need be.
	 */
	~Logger();
	/**
	 * Hidden for singleton
	 * @see Logger::getInstance()
	 */
	Logger(Logger const&) {};
	/**
	 * Hidden for singleton
	 * @see Logger::getInstance()
	 */
	Logger& operator=(Logger const&) {return *this;}

	/**
	 * Writes the log message to the log file.
	 * @param[in] message Message
	 * @param[in] severity Severity of the event
	 * @param[in] source Location we are logging from
	 */
	void logToFile(const std::string &message, int severity, const std::string &source = "");
	/**
	 * Writes the log message to standard out/error.
	 * @param[in] message Message
	 * @param[in] severity Severity of the event
	 * @param[in] source Location we are logging from
	 */
	void logToConsole(const std::string &message, int severity, const std::string &source = "");

	/**
	 * Writes the current time and date in YYYY-MM-DD HH:MM:SS into the stream.
	 * @param[out] os Stream
	 */
	static void writeCurrentTime(std::ostream &os);
};

#endif // __LOGGER_HPP__
