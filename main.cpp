/**
 * @file main.cpp
 * Contains the implementation of the main function and supporting functions.
 *
 * @author Petr Klima
 * @version 0.1
 */

// NOTE: Some parts within this file contain code that has been adapted from
//       Beej's Guide to Network Programming (http://beej.us/guide/bgnet/output/html/multipage/index.html)
//       Such parts are enclosed in START OF 3RD PARTY CODE ... END OF 3RD PARTY CODE blocks

#include <signal.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <cstdlib>
#include <unistd.h>

#include "Logger.hpp"
#include "MimeDetector.hpp"

#include "main.hpp"

Configuration *g_config = NULL; /**< Configuration container */
ThreadPool *g_threadPool = NULL; /**< Thread pool handler */

int processParameters(int argc, const char* argv[], int &verbosity, bool &mutexLog)
{
	verbosity = 0;
	mutexLog = false;

	if (argc < 2)
	{
		printUsage(argv[0]);
		return 0;
	}
	if (argv[1][0] == '-' || (argc == 3 && argv[2][0] != '-'))
	{
		printUsage(argv[0]);
		return 0;
	}
	if (argc == 3)
	{

		for (int i = 1; i < 8; i++)
		{
			char ch = argv[2][i];
			if (!ch)
				break;
			if (ch == '!')
			{
				if (argv[2][i+1])
				{
					printUsage(argv[0]);
					return EXIT_ERROR_PARAMETERS;
				}
				else
				{
					mutexLog = true;
					break;
				}
			}
			if (ch == 'v')
			{
				verbosity++;
			}
			else
			{
				printUsage(argv[0]);
				return EXIT_ERROR_PARAMETERS;
			}
		}
		if (verbosity > 4)
		{
			printUsage(argv[0]);
			return EXIT_ERROR_PARAMETERS;
		}
	}
	return 0;
}

int initializeConfigLogger(const char *configFile, int verbosity, bool mutexLog)
{
	IniConfig *ini = new IniConfig();
	try
	{
		if (!ini->loadFromFile(configFile))
		{
			std::cerr << "Could not open config file at " << configFile << std::endl;
			delete ini;
			return EXIT_ERROR_CONFIG;
		}
	}
	catch (IniParserException ex)
	{
		std::cerr << ex << std::endl;
		delete ini;
		return EXIT_ERROR_INIPARSER;
	}

	g_config = new Configuration();
	if (!g_config->loadConfiguration(ini))
	{
		return EXIT_ERROR_CONFIG;
	}

	if (!Logger::getInstance()->openFile(g_config->getLog(), verbosity, mutexLog))
	{
		std::cerr << "Cannot open log file \"" << g_config->getLog() << "\" for writing: " << strerror(errno) << std::endl;
		return EXIT_ERROR_LOG;
	}

	Logger::getInstance()->log("Started with configuration...", Logger::INFO0);

	std::ostringstream ss;

	ss << "Listen address: " << g_config->getListenAddress();
	Logger::getInstance()->log(ss.str(), Logger::INFO0);
	ss.str("");

	ss << "Listen port: " << g_config->getListenPort();
	Logger::getInstance()->log(ss.str(), Logger::INFO0);
	ss.str("");

	ss << "Cache capacity: " << g_config->getCacheCapacity() << " KiB";
	Logger::getInstance()->log(ss.str(), Logger::INFO0);
	ss.str("");

	ss << "Cache lifetime: " << g_config->getCacheLifetime() << " min";
	Logger::getInstance()->log(ss.str(), Logger::INFO0);
	ss.str("");

	ss << "Max connections: " << g_config->getMaxConnections();
	Logger::getInstance()->log(ss.str(), Logger::INFO0);
	ss.str("");

	ss << "Default server: " << g_config->getDefaultServer();
	Logger::getInstance()->log(ss.str(), Logger::INFO0);

	return 0;
}

void terminate(int returnCode)
{
	delete g_config;
	g_config = NULL;
	delete g_threadPool;
	g_threadPool = NULL;
	MimeDetector::destroy();

	if (Logger::getInstance())
	{
		if (returnCode == 0)
		{
			Logger::getInstance()->log("Shut down", Logger::INFO1);
		}
		else
		{
			std::ostringstream ss;
			ss << "Terminated with error code ";
			ss << returnCode;
			Logger::getInstance()->log(ss.str(), Logger::INFO0);
		}
	}

	Logger::destroy(); //Delete last

	exit(returnCode);
}

void reportError(const std::string &message)
{
	if (Logger::getInstance())
		Logger::getInstance()->log(message + ": " + strerror(errno), Logger::ERROR, "MAIN");
	else
		std::cerr << "MAIN" << message << ": " << strerror(errno) << std::endl;
}

void printUsage(const char *binName)
{
	std::cout << "Usage: " << binName << " pathToConfigIni [-v[v[v[v]]]][!]" << std::endl;
	std::cout << "\tpathToConfigIni - path to an INI file with server configuration" << std::endl;
	std::cout << "\t[-v[v[v[v]]]][!] - level of verbosity:" << std::endl;
	std::cout << "\t\t<not specified> - report errors to stderr, log errors and warnings" << std::endl;
	std::cout << "\t\t-v    - report errors and warnings to stderr, log errors, warnings and important info" << std::endl;
	std::cout << "\t\t-vv   - report errors and warnings to stderr, log errors, warnings, important and interesting info" << std::endl;
	std::cout << "\t\t-vvv  - report errors and warnings to stderr, log everything" << std::endl;
	std::cout << "\t\t-vvvv - report errors and warnings to stderr, log everything and report it to stdout as well" << std::endl;
	std::cout << "\t\t-v!, -vv!, -vvv!, -vvvv! - all logging and reporting will be mutexed, results in reliable logs but significant slowdown" << std::endl;
}

void signalHook(int signal)
{
	if (signal == SIGPIPE)
	{
		// Socket closed unexpectedly.
		Logger::getInstance()->log("Broken pipe, restarting thread...", Logger::ERROR);
		// Reset the thread to make sure it can accept work again.
		g_threadPool->resetThread(pthread_self());
		return;
	}
	if (!g_config) //Already terminating
		return;
	if (signal == SIGTERM)
		Logger::getInstance()->log("Caught signal SIGTERM", Logger::INFO0);
	else
		Logger::getInstance()->log("Caught signal SIGINT", Logger::INFO0);
	terminate(0);
}

void getIpAddress(sockaddr_storage *address, char result[INET6_ADDRSTRLEN])
{
	// ---------- START OF 3RD PARTY CODE ----------
	inet_ntop(address->ss_family,
	          ((address->ss_family == AF_INET) ?
	           (void*) &(((sockaddr_in*) address)->sin_addr) :
	           (void*) &(((sockaddr_in6*) address)->sin6_addr)
	          ),
	          result,
	          INET6_ADDRSTRLEN
	         );
	// ---------- END OF 3RD PARTY CODE ----------
}

int main(int argc, const char* argv[])
{
	int verbosity;
	bool mutexLog;
	int ret = processParameters(argc, argv, verbosity, mutexLog);
	if (ret)
		return ret;

	//Initialize config
	ret = initializeConfigLogger(argv[1], verbosity, mutexLog);
	if (ret)
		terminate(ret);

	// Hook to SIGINT and SIGTERM signals
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = signalHook;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGPIPE, &action, NULL); //In case of socket write failiure

	// ---------- START OF 3RD PARTY CODE ----------
	addrinfo hints, *serverInfo;
	int listenSockedFd;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; //Use my IP

	int rc = getaddrinfo(g_config->getListenAddress().c_str(), g_config->getListenPort().c_str(), &hints, &serverInfo); //Fill in the info
	if (rc != 0)
	{
		std::ostringstream ss;
		ss << "Could not resolve listen address: " << gai_strerror(rc);
		Logger::getInstance()->log(ss.str(), Logger::ERROR, "MAIN");
		terminate(EXIT_ERROR_BIND);
	}

	// Loop through all the results and bind to the first we can
	addrinfo *info;
	for (info = serverInfo; info != NULL; info = info->ai_next)
	{
		// Open socket to listen on
		listenSockedFd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
		if (listenSockedFd == -1)
		{
			reportError("Could not open listen socket");
			continue;
		}

		// Set socket options
		int isReusable = 1;
		if (setsockopt(listenSockedFd, SOL_SOCKET, SO_REUSEADDR,
		               (void*) &isReusable, sizeof(int)) == -1)
		{
			reportError("Could not set socket options");
			terminate(EXIT_ERROR_BIND);
		}

		// Bind the socket to the port
		if (bind(listenSockedFd, info->ai_addr, info->ai_addrlen) == -1)
		{
			close(listenSockedFd);
			reportError("Could not bind listen port");

			continue;
		}

		break;
	}

	if (info == NULL)
	{
		Logger::getInstance()->log("Failed to bind anything", Logger::ERROR, "MAIN");
		terminate(EXIT_ERROR_BIND);
	}

	freeaddrinfo(serverInfo);

	// Start listening
	if (listen(listenSockedFd, 10) == -1)
	{
		reportError("Could not listen");
		terminate(EXIT_ERROR_LISTEN);
	}
	// ---------- END OF 3RD PARTY CODE ----------

	g_threadPool = new ThreadPool(g_config->getMaxConnections(), g_config);
	if (!g_threadPool->initialize())
		terminate(EXIT_ERROR_THREADS);

	sockaddr_storage clientAddress;
	socklen_t cli_addrLen = sizeof(clientAddress);
	while (true)
	{
		Logger::getInstance()->log("Waiting for a connection", Logger::INFO2, "MAIN");

		// Wait and accept a connection
		int connSocketFd = accept(listenSockedFd, (sockaddr*) & clientAddress, &cli_addrLen);
		if (connSocketFd == -1)
		{
			reportError("Could not accept connection");
			continue;
		}

		char ipStr[INET6_ADDRSTRLEN];
		memset(ipStr, 0, INET6_ADDRSTRLEN);
		getIpAddress(&clientAddress, ipStr);

		std::ostringstream ss;
		ss << "Established connection with " << ipStr;
		Logger::getInstance()->log(ss.str(), Logger::INFO1, "MAIN");

		g_threadPool->delegateWork(connSocketFd);
	}

	return 0;
}
