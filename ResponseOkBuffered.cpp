/**
 * @file ResponseOkBuffered.cpp
 * Contains implementation of class ResponseOkBuffered.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseOkBuffered.hpp"

ResponseOkBuffered::ResponseOkBuffered(char* content, long unsigned int contentLength, long unsigned int etag, const char* contentType) :
		ResponseOk(contentLength, contentType),
		content(content), etag(etag)
{
}

bool ResponseOkBuffered::writeHeader(FILE* stream) const
{
	return ((!etag ||
	         (etag && fprintf(stream, "\r\nETag: \"%ld\"", etag) > 0)
	        ) &&
	        ResponseOk::writeHeader(stream));
}
bool ResponseOkBuffered::writeContent(FILE* stream)
{
	if (content)
		return fwrite(content, sizeof(*content), contentLength, stream) == contentLength;
	else
		return true;
}
