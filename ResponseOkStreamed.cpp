/**
 * @file ResponseOkStreamed.cpp
 * Contains implementation of class ResponseOkStreamed.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseOkStreamed.hpp"

#include "MimeDetector.hpp"

ResponseOkStreamed::ResponseOkStreamed(const std::string& source, long unsigned int contentLength):
	ResponseOk(contentLength, ""),
	fileStream(source.c_str(), std::ios::binary)
{
	contentType = MimeDetector::getInstance()->detect(source, fileStream);
}
ResponseOkStreamed::~ResponseOkStreamed()
{
	fileStream.close();
}

bool ResponseOkStreamed::writeContent(FILE* stream)
{
	if (!fileStream.is_open())
		return false;

	fileStream.seekg(std::ios::beg);
	int ch;
	while ((ch = fileStream.get()) != EOF)
	{
		if (fputc(ch, stream) == EOF)
			return false;
	}
	return true;
}
