/**
 * @file Request.hpp
 * Contains definition of class Request.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __REQUEST_HPP__
#define __REQUEST_HPP__

#include <string>

/**
 * An HTTP request.
 * Represents an HTTP request received from the client.
 */
class Request
{
public:
	/**
	 * HTTP request method.
	 */
	enum Method
	{
		GET, /**< GET method - request for content of a file  */
		HEAD, /**< HEAD method - request for header corresponding to a file */
		OPTIONS /**< OPTIONS method - request for a list of supported methods */
	};

	static const int OK = 0; /**< Request processed OK */
	static const int INVALID = -1; /**< Request is invalid HTTP */
	static const int UNSUPPORTED = -2; /**< Request is HTTP-valid but the server does not support it */
private:
	std::string path; /**< Requested path */
	std::string host; /**< Virtual host */
	unsigned long etag; /**< HTTP Etag */
	Method method; /**< HTTP request method */

	static const unsigned int URI_LIMIT = 1000; /**< Maximal URI length */
public:
	/**
	 * Processes the incomming stream.
	 * @param[in] stream
	 * @return OK, INVALID or UNSUPPORTED
	 * @see Request::OK
	 * @see Request::INVALID
	 * @see Request::UNSUPPORTED
	 */
	int process(FILE *stream);
	/**
	 * Returns name of the file that was requested.
	 * @return Requested file
	 */
	std::string getRequestedPath() const;
	/**
	 * Returns name of the virtual host that the request was aimed at.
	 * @return Virtual host
	 */
	std::string getRequestedHost() const;
	/**
	 * Returns the ETag specified by If-None-Match.
	 * @return ETag or 0 if none was specified
	 */
	unsigned int getETag() const;
	/**
	 * Returns the request's Method.
	 * @return HTTP method
	 */
	Method getMethod() const;
private:
	/**
	 * Removes port part from the host address.
	 */
	void trimHost();
	/**
	 * Retrieves a word from the stream separated by space.
	 * @param[in] stream Stream
	 * @param[out] word Resulting word
	 * @param[out] end true if we reached CLRF
	 * @param[in] limit Max number of characters read
	 * @return false if limit was reached
	 */
	static bool getWord(FILE* stream, std::string& word, bool& end, unsigned int limit = 0);
	/**
	 * Retrieves a line from the stream.
	 * @param[in] stream Stream
	 * @param[out] line Resulting line
	 * @param[out] end true if we reached EOF
	 * @param[in] limit Max number of characters read
	 * @return false if limit was reached
	 */
	static bool getLine(FILE* stream, std::string& line, bool& end, unsigned int limit = 0);
	/**
	 * Checks the path for directory traversal exploit.
	 * @param[in] path Path to check
	 * @return Path OK
	 */public:
	static bool pathValid(const std::string &path);
public:
	/**
	 * Decodes the percent-encoded character.
	 * @param[in] d1 First digit
	 * @param[in] d2 Second digit
	 * @return Decoded character
	 */
	static char urlDecode(char d1, char d2);
	/**
	 * Decodes the percent-encoded string
	 * @param[in] encoded Percent-encoded string
	 * @returns Decoded string
	 */
	static std::string urlDecode(const std::string& encoded);
};

#endif // __REQUEST_HPP__
