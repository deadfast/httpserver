/**
 * @file Configuration.cpp
 * Contains implementation of class Configuration.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "Configuration.hpp"

Configuration::Configuration() : iniConfig(NULL)
{
}
Configuration::~Configuration()
{
	delete iniConfig;
}

bool Configuration::loadConfiguration(IniConfig *ini)
{
	iniConfig = ini;
	bool error = false;
	IniConfig::Section *root = ini->getSection("");

	try
	{
		listenAddress = root->getEntry("listenAddress");
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("listenAddress");
		error = true;
	}

	try
	{
		unsigned int dummy;
		if (!IniConfig::convertToUInt(root->getEntry("listenPort"), dummy))
		{
			printUIntError("listenPort");
			error = true;
		}
		else if (dummy == 0)
		{
			printZeroError("listenPort");
			error = true;
		}
		listenPort = root->getEntry("listenPort");
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("listenPort");
		error = true;
	}

	try
	{
		if (!IniConfig::convertToUInt(root->getEntry("cacheCapacity"), cacheCapacity))
		{
			printUIntError("cacheCapacity");
			error = true;
		}
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("cacheCapacity");
		error = true;
	}

	try
	{
		if (!IniConfig::convertToUInt(root->getEntry("cacheLifetime"), cacheLifetime))
		{
			printUIntError("cacheLifetime");
			error = true;
		}
		else if (cacheLifetime == 0)
		{
			printZeroError("cacheLifetime");
			error = true;
		}
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("cacheLifetime");
		error = true;
	}

	try
	{
		if (!IniConfig::convertToUInt(root->getEntry("maxConnections"), maxConnections))
		{
			printUIntError("maxConnections");
			error = true;
		}
		else if (maxConnections == 0)
		{
			printZeroError("maxConnections");
			error = true;
		}
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("maxConnections");
		error = true;
	}

	try
	{
		log = root->getEntry("log");
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("listenPort");
		error = true;
	}

	try
	{
		defaultServer = root->getEntry("defaultServer");
		try
		{
			ini->getSection(defaultServer)->getEntry("root");
		}
		catch (UnknownIniSectionException)
		{
			std::cerr << "Error processing config file: defaultServer points to a non-existant section \'" << defaultServer << "\'" << std::endl;
			error = true;
		}
		catch (UnknownIniEntryException)
		{
			std::cerr << "Error processing config file: section pointed to by defaultServer lacks a 'root' entry" << std::endl;
			error = true;
		}
	}
	catch (UnknownIniEntryException)
	{
		printMissingError("defaultServer");
		error = true;
	}

	return !error;
}

std::string Configuration::getListenAddress() const
{
	return listenAddress;
}
std::string Configuration::getListenPort() const
{
	return listenPort;
}
unsigned int Configuration::getCacheCapacity() const
{
	return cacheCapacity;
}
unsigned int Configuration::getCacheLifetime() const
{
	return cacheLifetime;
}
unsigned int Configuration::getMaxConnections() const
{
	return maxConnections;
}
std::string Configuration::getLog() const
{
	return log;
}
std::string Configuration::getDefaultServer() const
{
	return defaultServer;
}

int Configuration::getServerPath(const std::string& name, std::string& path) const
{
	try
	{
		std::string root = iniConfig->getSection(name)->getEntry("root");
		if (root.length() == 0)
			return -1;
		if (root[root.length()-1] == '/') //Remove trailing '/'
			path = root.substr(0, root.length() - 1);
		else
			path = root;
		return 1;
	}
	catch (UnknownIniSectionException)
	{
		return 0;
	}
	catch (UnknownIniEntryException)
	{
		return -1;
	}
}

void Configuration::printUIntError(const char* name)
{
	std::cerr << "Error processing config file: " << name << " is not a positive number" << std::endl;
}
void Configuration::printZeroError(const char* name)
{
	std::cerr << "Error processing config file: " << name << " cannot be 0" << std::endl;
}
void Configuration::printMissingError(const char* name)
{
	std::cerr << "Error processing config file: entry " << name << " is missing" << std::endl;
}
