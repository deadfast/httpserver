/**
 * @file ResponseOptions.hpp
 * Contains definition of class ResponseOptions.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEOPTIONS_HPP__
#define __RESPONSEOPTIONS_HPP__

#include "Response.hpp"

/**
 * Response to an <i>OPTIONS</i> request.
 */
class ResponseOptions : public Response
{
public:
	ResponseOptions();
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEOPTIONS_HPP__
