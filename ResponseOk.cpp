/**
 * @file ResponseOk.cpp
 * Contains implementation of class ResponseOk.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseOk.hpp"

ResponseOk::ResponseOk(long unsigned int contentLength, const char* contentType) :
		ResponseWithContent("200 OK", contentLength, contentType)
{
}
