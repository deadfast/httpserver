/**
 * @mainpage
 * @author Petr Klima
 * @version 0.1
 * @date 2012-05-29
 *
 * @section summary Summary:
 * A simple, lightweigth HTTP/1.1-compliant HTTP *nix server.<br>
 * Supports sliding file caching via strong ETag implementation.<br>
 * Configurable using an external INI configuration file.
 * @section support Supported HTTP methods:
 * - GET
 * - HEAD
 * - OPTIONS
 * @section config Configuration
 * The server is configured using an external INI file, path to which has to be then passed onto the server as the first command-line parameter.<br>
 * An example <i>config.ini</i> is included.<br>
 * @subsection configRequired Required settings
 * The INI file has to contain the following settings:
 * - <b>listenAddress</b> - The address to listen on. Can be a hostname or an IPv4/IPv6 address.
 * - <b>listenPort</b> - The port the server will listen on for incomming connections.
 * - <b>cacheCapacity</b> - Capacity of the cache in kibibytes (KiB). If set to 0, the cache will be disabled.
 * - <b>cacheLifetime</b> - Time since the last access before an item is removed from the cache (in minutes).
 * - <b>maxConnections</b> - Number of maximal simultaneous connections.
 * - <b>log</b> - Path to the log file.
 * - <b>defaultServer</b> - Default virtual server to use for HTTP/1.0 connections where no explicit <i>Host</i> is specified.
 * - <b>[alias.virtualserver.com]</b> - Virtual server defined as a section.
 *   - <b>root</b> - Path to a directory that will be considered the server's root.
 * @subsection configParser INI syntax
 * - Blank lines are allowed.
 * - No whitespace between property name and the equals sign allowed.
 * - Whitespace between equals sign and property value will be considered a part of the property value.
 * - Comment lines are allowed, are denoted using a semicolon character (;) which has to be the first character on that line.
 * @subsection configExample Example
 * <code>
 * ; Address to listen on for incomming connections<br>
 * listenAddress=localhost<br>
 * ; Port to listen on for incomming connections<br>
 * listenPort=8000<br>
 * ; Cache size in kibibytes (0 to disable cache)<br>
 * cacheCapacity=4<br>
 * ; Time (minutes) before item in cache expires<br>
 * cacheLifetime=1<br>
 * ; Number of maximal simultaneous connections<br>
 * maxConnections=100<br>
 * ; Log file<br>
 * log=examples/http.log<br>
 * ; Virtual server to default to for HTTP/1.0 connections<br>
 * defaultServer=localhost<br>
 * <br>
 * ; Virtual servers<br>
 * [localhost]<br>
 * root=./examples/www/main<br>
 * <br>
 * [doc.localhost]<br>
 * root=./examples/www/doc
 * </code>
 */