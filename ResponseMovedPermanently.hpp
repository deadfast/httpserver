/**
 * @file ResponseMovedPermanently.hpp
 * Contains definition of class ResponseMovedPermanently.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEMOVEDPERMANENTLY_HPP__
#define __RESPONSEMOVEDPERMANENTLY_HPP__

#include "ResponseWithContent.hpp"

#include <string>

/**
 * A <i>301 Moved Permanently</i> response.
 */
class ResponseMovedPermanently : public ResponseWithContent
{
private:
	std::string movedTo; /**< Location to redirect to */
	static const char* text; /**< Text to display on the redirect page */
public:
	/**
	 * Creates a Moved Permanently response with redirection to the given URL.
	 * @param[in] movedTo Redirect URL
	 */
	ResponseMovedPermanently(const std::string &movedTo);
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEMOVEDPERMANENTLY_HPP__
