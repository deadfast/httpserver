/**
 * @file IniConfig.cpp
 * Contains implementation of class IniConfig and IniConfig::Section.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "IniConfig.hpp"

#include <cstdio>
#include <fstream>
#include <sstream>

IniConfig::Section::Section()
{
}
bool IniConfig::Section::addEntry(const std::pair< std::string, std::string >& entry)
{
	return entries.insert(entry).second;
}
std::string& IniConfig::Section::getEntry(const std::string& name) throw(UnknownIniEntryException)
{
	std::map<std::string, std::string>::iterator it = entries.find(name);
	if (it == entries.end())
		throw UnknownIniEntryException(name);
	return it->second;
}
void IniConfig::Section::print(std::ostream& os) const
{
	for (std::map<std::string, std::string>::const_iterator it = entries.begin(); it != entries.end(); it++)
		os << (*it).first << "=" << (*it).second << std::endl;
}
std::ostream& operator<<(std::ostream &os, const IniConfig::Section &section)
{
	section.print(os);
	return os;
}


IniConfig::IniConfig()
{
}
IniConfig::~IniConfig()
{
	for (std::map<std::string, Section*>::const_iterator it = sections.begin(); it != sections.end(); it++)
		delete(*it).second;
}

bool IniConfig::addSection(const std::pair< std::string, IniConfig::Section* >& section)
{
	return sections.insert(section).second;
}
IniConfig::Section* IniConfig::getSection(const std::string& name) throw(UnknownIniSectionException)
{
	std::map<std::string, Section*>::iterator it = sections.find(name);
	if (it == sections.end())
		throw UnknownIniSectionException(name);
	return it->second;
}

void IniConfig::print(std::ostream& os) const
{
	for (std::map<std::string, Section*>::const_iterator it = sections.begin(); it != sections.end(); it++)
	{
		if ((*it).first.length() > 0)
			os << "[" << (*it).first << "]" << std::endl;
		os << *((*it).second);
	}
}
std::ostream& operator<<(std::ostream &os, const IniConfig &cfg)
{
	cfg.print(os);
	return os;
}

bool IniConfig::loadFromFile(const std::string fileName) throw(IniParserException)
{
	return loadFromFile(fileName.c_str());
}
bool IniConfig::loadFromFile(const char *fileName) throw(IniParserException)
{
	std::ifstream file(fileName);
	if (!file.is_open())
		return false;

	int lineNum = 1, charNum = 0;
	std::string name = "", value = "";
	bool readName = true, readSection = false, ignoreLine = false;
	Section *section = new Section();
	sections.insert(std::pair<std::string, Section*>("", section));

	while (file.good() || file.eof())
	{
		charNum++;
		char c;
		file >> std::noskipws >> c;
		int ch = (file.eof()) ? EOF : c;

		if (ch == '\n' || ch == EOF)
		{
			if (!ignoreLine && charNum != 1) //Not skipping line and not a blank line
			{
				if (readName) //Reading the entry name
				{
					throw IniParserException(fileName, lineNum, charNum, "Encountered \'\\n\', expected \'=\'");
				}
				else if (readSection) //Reading the section name
				{
					if (!section)
						throw IniParserException(fileName, lineNum, charNum, "Encountered \'\\n\', expected \']\'");
				}
				else //Reading the value
				{
					if (name.length() == 0)
					{
						throw IniParserException(fileName, lineNum, 0, "Missing entry name");
					}
					if (value.length() == 0)
					{
						throw IniParserException(fileName, lineNum, 0, "Entry \'" + name + "\' has no value");
					}
					if (!section->addEntry(std::pair<std::string, std::string>(name, value)))
					{
						throw IniParserException(fileName, lineNum, 0, "Duplicate entry \'" + name + "\'");
					}
				}
			}
			if (ch == EOF)
				break;
			readName = true;
			readSection = false;
			ignoreLine = false;
			name = "";
			value = "";
			lineNum++;
			charNum = 0;
			continue; //Skip the rest
		}

		if (!ignoreLine) //We're not in a comment
		{
			if (charNum == 1) //First character on the line
			{
				if (ch == '[') //Start of a section
				{
					readName = false;
					readSection = true;
					section = NULL;
					continue; //Skip this character
				}
				else if (ch == ';') //Comment line
				{
					ignoreLine = true;
					continue; //Skip
				}
			}
			if (readName) //Reading the entry name
			{
				if (std::isalnum((char) ch, std::locale("C"))) //Read an alphanumeric character
				{
					name += (char) ch;
				}
				else if (ch == '=') //Read a =, value should follow
				{
					readName = false;
				}
				else
				{
					throw IniParserException(fileName, lineNum, charNum, std::string("Encountered \'") + (char) ch + "\', expected \'=\'");
				}
			}
			else if (readSection) //Reading the section name
			{
				if (section) //The section name was loaded
				{
					throw IniParserException(fileName, lineNum, charNum, std::string("Encountered \'") + (char) ch + "\', expected \'\\n\'");
				}
				else
				{
					if (ch == ']')
					{
						if (name.length() == 0)
						{
							throw IniParserException(fileName, lineNum, 0, "Empty section name");
						}
						else
						{
							section = new Section();
							if (!sections.insert(std::pair<std::string, Section*>(name, section)).second)
							{
								delete section;
								throw IniParserException(fileName, lineNum, 0, "Duplicate section \'" + name + "\'");
							}
						}
					}
					else if (std::isprint((char) ch, std::locale("C"))) //Read an alphanumeric character
					{
						name += (char) ch;
					}
					else
					{
						throw IniParserException(fileName, lineNum, charNum, std::string("Encountered \'") + (char) ch + "\', expected \']\'");
					}
				}
			}
			else //Reading the entry value
			{
				value += (char) ch;
			}
		}
	}

	return true;
}

bool IniConfig::writeToFile(const std::string fileName)
{
	return writeToFile(fileName.c_str());
}
bool IniConfig::writeToFile(const char* fileName)
{
	std::ofstream file(fileName);
	if (!file.is_open())
		return false;

	print(file);
	return true;
}
bool IniConfig::convertToUInt(const std::string& str, unsigned int& result)
{
	unsigned int num;
	std::stringstream ss(str);
	ss >> num;
	char dummy;
	if (ss.fail() || ss.get(dummy))
		return false;

	result = num;
	return true;
}
