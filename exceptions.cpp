/**
 * @file exceptions.cpp
 * Contains implementation of Exception, FileIOException, IniParserException, UnknownIniSectionException and UnknownIniEntryException classes.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "exceptions.hpp"

std::ostream& operator<<(std::ostream &os, const Exception &ex)
{
	ex.print(os);
	return os;
}

Exception::~Exception()
{
}

FileIOException::FileIOException(const std::string& fileName, const std::string& reason) :
		fileName(fileName), reason(reason)
{

}
FileIOException::FileIOException(const std::string& fileName, const char* reason) :
		fileName(fileName), reason(reason)
{

}
FileIOException::FileIOException(const char* fileName, const std::string& reason) :
		fileName(fileName), reason(reason)
{

}
FileIOException::FileIOException(const char* fileName, const char* reason) :
		fileName(fileName), reason(reason)
{

}

void FileIOException::print(std::ostream& os) const
{
	os << "FileIOException: " << fileName << ": " << reason;
}

IniParserException::IniParserException(const std::string& fileName, unsigned int line, unsigned int character, const std::string& reason) :
		fileName(fileName), line(line), character(character), reason(reason)
{
}
IniParserException::IniParserException(const std::string& fileName, unsigned int line, unsigned int character, const char *reason) :
		fileName(fileName), line(line), character(character), reason(reason)
{
}
void IniParserException::print(std::ostream& os) const
{
	os << fileName;
	if (line > 0)
	{
		os << ":" << line;
		if (character > 0)
		{
			os << ":" << character;
		}
	}
	if (reason.length() > 0)
	{
		os << " - " << reason;
	}
}

UnknownIniSectionException::UnknownIniSectionException(const std::string& section) :
		section(section)
{
}
UnknownIniSectionException::UnknownIniSectionException(const char* section) :
		section(section)
{
}
void UnknownIniSectionException::print(std::ostream& os) const
{
	os << "UnknownIniSectionException: \"" << section << "\"";
}

UnknownIniEntryException::UnknownIniEntryException(const std::string& entry) :
		entry(entry)
{
}
UnknownIniEntryException::UnknownIniEntryException(const char* entry) :
		entry(entry)
{
}
void UnknownIniEntryException::print(std::ostream& os) const
{
	os << "UnknownIniEntryException: \"" << entry << "\"";
}
