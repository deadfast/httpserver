/**
 * @file exceptions.hpp
 * Contains definition of Exception, FileIOException, IniParserException, UnknownIniSectionException and UnknownIniEntryException classes.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __EXCEPTIONS_HPP__
#define __EXCEPTIONS_HPP__

#include <string>
#include <iostream>

/**
 * An exception.
 *
 * An abstract base for exceptions.
 */
class Exception
{
public:
	virtual ~Exception();

	/**
	 * Prints the exception to the stream.
	 * @param[in,out] os stream
	 */
	virtual void print(std::ostream &os) const = 0;
	/**
	 * Implements the << stream operator.
	 * @param[out] os Output stream
	 * @param[in] ex Exception to print
	 * @return Output stream
	 */
	friend std::ostream& operator<<(std::ostream &os, const Exception &ex);
};

/**
 * An exception thrown in case of a file I/O call error.
 */
class FileIOException : public Exception
{
private:
	std::string fileName; /**< File name */
	std::string reason; /**< Reason for the error */
public:
	/**
	 * Creates a file I/O call exception.
	 * @param[in] fileName File name
	 * @param[in] reason Reason for the exception
	 */
	FileIOException(const std::string &fileName, const std::string &reason);
	/**
	 * @copydoc FileIOException::FileIOException()
	 */
	FileIOException(const std::string &fileName, const char *reason);
	/**
	 * @copydoc FileIOException::FileIOException()
	 */
	FileIOException(const char *fileName, const std::string &reason);
	/**
	 * @copydoc FileIOException::FileIOException()
	 */
	FileIOException(const char *fileName, const char *reason);

	virtual void print(std::ostream &os) const;
};

/**
 * An exception thrown in case of INI file parsing error.
 */
class IniParserException : public Exception
{
protected:
	std::string fileName; /**< File name */
	unsigned int line; /**< Number of line where the error occured */
	unsigned int character; /**< Number of character where the error occured */
	std::string reason; /**< Reason for the error */
public:
	/**
	 * Creates an INI file parsing exception.
	 * @param[in] fileName File name
	 * @param[in] line Line number where the error occured
	 * @param[in] character Character number on which the error occured
	 * @param[in] reason Reason behind the error
	 */
	IniParserException(const std::string &fileName, unsigned int line, unsigned int character, const std::string &reason);
	/**
	 * @copydoc IniParserException::IniParserException()
	 */
	IniParserException(const std::string &fileName, unsigned int line, unsigned int character, const char *reason);

	void print(std::ostream &os) const;
};

/**
 * An exception thrown when a nonexistent INI config section is requested.
 */
class UnknownIniSectionException : public Exception
{
private:
	std::string section; /**< Section name */
public:
	/**
	 * Creates an unknown INI section exception.
	 * @param[in] section Name of the section
	 */
	UnknownIniSectionException(const std::string &section);
	/**
	 * @copydoc UnknownIniSectionException::UnknownIniSectionException()
	 */
	UnknownIniSectionException(const char *section);

	void print(std::ostream &os) const;
};
/**
 * An exception thrown when a nonexistent INI config entry is requested.
 */
class UnknownIniEntryException : public Exception
{
private:
	std::string entry; /**< Entry name */
public:
	/**
	 * Creates an unknown INI entry exception.
	 * @param[in] entry Name of the entry
	 */
	UnknownIniEntryException(const std::string &entry);
	/**
	 * @copydoc UnknownIniEntryException::UnknownIniEntryException()
	 */
	UnknownIniEntryException(const char *entry);

	void print(std::ostream &os) const;
};

#endif // __EXCEPTIONS_HPP__
