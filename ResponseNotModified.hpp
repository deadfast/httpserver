/**
 * @file ResponseNotModified.hpp
 * Contains definition of class ResponseNotModified.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSENOTMODIFIED_HPP__
#define __RESPONSENOTMODIFIED_HPP__

#include "Response.hpp"

/**
 * A <i>304 Not Modified</i> response.
 */
class ResponseNotModified : public Response
{
public:
	ResponseNotModified();
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSENOTMODIFIED_HPP__
