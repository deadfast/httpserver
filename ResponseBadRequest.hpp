/**
 * @file ResponseBadRequest.hpp
 * Contains definition of class ResponseBadRequest.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEBADREQUEST_HPP__
#define __RESPONSEBADREQUEST_HPP__

#include "Response.hpp"

/**
 * A <i>400 Bad Request</i> response.
 */
class ResponseBadRequest : public Response
{
public:
	ResponseBadRequest();
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEBADREQUEST_HPP__
