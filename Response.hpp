/**
 * @file Response.hpp
 * Contains definition of class Response.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSE_HPP__
#define __RESPONSE_HPP__

#include <ctime>
#include <cstdio>

/**
 * An abstract base for a response to an HTTP request.
 */
class Response
{
private:
	const char *responseName; /**< Name of the HTTP response. */
protected:
	/**
	 * Creates a response with the given name.
	 * @param[in] responseName name of the HTTP response
	 */
	Response(const char *responseName);
public:
	virtual ~Response();

	/**
	 * Writes itself into the connection socket.
	 * @param[in] socketFd Socket file descriptor
	 * @return Success
	 */
	bool respond(int socketFd);
private:
	/**
	 * Writes the common header into the stream.
	 * @param[in] stream Socket stream
	 * @return Success
	 */
	bool writeCommonHeader(FILE *stream) const;
	/**
	 * Writes the HTTP response code and name into the stream.
	 * @param[in] stream Socket stream
	 * @return Success
	 */
	virtual bool writeResponseName(FILE *stream);
protected:
	/**
	 * Writes the response's header into the stream.
	 * @param[in] stream Socket stream
	 * @return Success
	 */
	virtual bool writeHeader(FILE *stream) const = 0;
	/**
	 * Writes the response's content into the stream.
	 * @param[in] stream Socket stream
	 * @return Success
	 */
	virtual bool writeContent(FILE *stream) = 0;

	/**
	 * Writes the supplied date and time into the string in a 33 characters long RFC 1123 compliant format
	 * @param[in] dateTime Date and time information
	 * @param[out] str Destination string
	 */
	static void writeDate(const time_t &dateTime, char *str);
	/**
	 * Writes the current date and time into the string in a 33 characters long RFC 1123 compliant format
	 * @param[out] str Destination string
	 */
	static void writeDate(char *str);
};

#endif // __RESPONSE_HPP__
