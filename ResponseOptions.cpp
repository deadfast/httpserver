/**
 * @file ResponseOptions.cpp
 * Contains implementation of class ResponseOptions.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseOptions.hpp"

ResponseOptions::ResponseOptions() :
		Response("400 OK")
{
}
bool ResponseOptions::writeHeader(FILE* stream) const
{
	return fputs("\r\nAllow: GET, HEAD, OPTIONS", stream) != EOF;
}
bool ResponseOptions::writeContent(FILE* stream)
{
	return true;
}
