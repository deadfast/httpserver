/**
 * @file ResponseMethodNotAllowed.cpp
 * Contains implementation of class ResponseMethodNotAllowed.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseMethodNotAllowed.hpp"

ResponseMethodNotAllowed::ResponseMethodNotAllowed() :
		Response("405 Method Not Allowed")
{
}

bool ResponseMethodNotAllowed::writeHeader(FILE* stream) const
{
	return fputs("\r\nAllow: GET, HEAD, OPTIONS", stream) != EOF;
}
bool ResponseMethodNotAllowed::writeContent(FILE* stream)
{
	return true;
}
