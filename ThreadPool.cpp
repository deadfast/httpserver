/**
 * @file ThreadPool.cpp
 * Contains implementation of class ThreadPool and structure ThreadPool::Thread.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ThreadPool.hpp"

#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include "Logger.hpp"
#include "ResponseInternalServerError.hpp"
#include "ResponseBadRequest.hpp"
#include "ResponseMethodNotAllowed.hpp"

ThreadPool::Thread::Thread() :
		id(-1), socketFd(IDLE), threadPool(NULL)
{
}

int ThreadPool::Thread::start(int id, ThreadPool* threadPool)
{
	this->id = id;
	this->threadPool = threadPool;
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&condition, NULL);
	return pthread_create(&thread, NULL, ThreadPool::workerThread, (void*) this);
}
void ThreadPool::Thread::stop()
{
	pthread_mutex_lock(&mutex);
	socketFd = Thread::ENDING;
	pthread_cond_signal(&condition);
	pthread_mutex_unlock(&mutex);

	pthread_join(thread, NULL);
}


ThreadPool::ThreadPool(unsigned int maxThreads, Configuration* config) :
		threads(new Thread[maxThreads]), maxThreads(maxThreads),
		config(config),
		cache(config)
{
}
ThreadPool::~ThreadPool()
{
	for (unsigned int i = 0; i < maxThreads; i++)
	{
		std::ostringstream ss;
		ss << "Terminating thread " << i << "...";
		Logger::getInstance()->log(ss.str(), Logger::INFO2, "ThreadPool::~ThreadPool");
		threads[i].stop();
	}
	delete[] threads;
}

bool ThreadPool::startThread(unsigned int id)
{
	int rc = threads[id].start(id, this);
	if (rc)
	{
		std::ostringstream ss;
		ss << "Failed to start a thread, pthread_create returned " << rc << ": " << strerror(errno);
		Logger::getInstance()->log(ss.str(), Logger::ERROR, "ThreadPool::startThread");
		return false;
	}
	
	return true;
}

bool ThreadPool::initialize()
{
	std::ostringstream ss;
	ss << "Starting " << maxThreads << " threads";
	Logger::getInstance()->log(ss.str(), Logger::INFO0, "ThreadPool::initialize");
	for (unsigned int i = 0; i < maxThreads; i++)
	{
		if (!startThread(i))
		{
			maxThreads = i;
			return false;
		}
	}

	return cache.initialize();
}
bool ThreadPool::delegateWork(int socketFd)
{
	int idle = findIdle();
	if (idle == -1) //Failed to find an idle thread
	{
		Logger::getInstance()->log("Thread limit reached, unable to process request", Logger::ERROR, "ThreadPool::delegateWork");
		char *msg = (char*) "Maximal number of connections reached";
		ResponseInternalServerError r(msg);

		r.respond(socketFd);
		close(socketFd);

		return false;
	}

	std::ostringstream ss;
	ss << "Delegate work to thread #" << idle;
	Logger::getInstance()->log(ss.str(), Logger::INFO2, "ThreadPool::delegateWork");
	pthread_mutex_lock(&(threads[idle].mutex));
	threads[idle].socketFd = socketFd;
	pthread_cond_signal(&(threads[idle].condition));
	pthread_mutex_unlock(&(threads[idle].mutex));

	return true;
}
int ThreadPool::findIdle()
{
	for (unsigned int i = 0; i < maxThreads; i++)
	{
		pthread_mutex_lock(&threads[i].mutex);
		bool found = (threads[i].socketFd == Thread::IDLE);
		pthread_mutex_unlock(&threads[i].mutex);
		if (found)
			return i;
	}

	return -1;
}

void ThreadPool::resetThread(pthread_t ptid)
{
	for (unsigned int i = 0; i < maxThreads; i++)
	{
		if (threads[i].thread == ptid)
		{
			threads[i].socketFd = Thread::IDLE;
			std::stringstream ss;
			ss << "Thread ID " << i << " was reset.";
			Logger::getInstance()->log(ss.str(), Logger::INFO0, "ThreadPool::resetThread");
		}
	}
}

void* ThreadPool::workerThread(void* in)
{
	Thread *data = (Thread*) in;
	std::ostringstream name;
	name << "Thread #" << data->id;
	while (true)
	{
		pthread_mutex_lock(&data->mutex);
		pthread_cond_wait(&data->condition, &data->mutex);
		pthread_mutex_unlock(&data->mutex);

		if (data->socketFd == Thread::ENDING)
			break;

		Logger::getInstance()->log("Processing work", Logger::INFO2, name.str());

		FILE *connection = fdopen(data->socketFd, "r");
		if (!connection)
		{
			std::ostringstream msg;
			msg << "Cannot open incomming socket: " << strerror(errno);
			Logger::getInstance()->log(msg.str(), Logger::ERROR, name.str());

			pthread_mutex_lock(&data->mutex);
			data->socketFd = -1;
			Logger::getInstance()->log("Finished work", Logger::INFO2, name.str());
			pthread_mutex_unlock(&data->mutex);
			continue;
		}

		Request req;
		int result = req.process(connection);

		Response *resp;
		if (result == Request::INVALID)
		{
			resp = new ResponseBadRequest();
			Logger::getInstance()->log("Request: INVALID", Logger::INFO2, name.str());
		}
		else if (result == Request::UNSUPPORTED)
		{
			resp = new ResponseMethodNotAllowed();
			Logger::getInstance()->log("Request: UNSUPPORTED", Logger::INFO2, name.str());
		}
		else
		{
			std::ostringstream ss;
			ss << "Request: " << ((req.getMethod() == Request::GET) ? "GET" : ((req.getMethod() == Request::HEAD) ? "HEAD" : "OPTIONS")) << " " << req.getRequestedPath();
			Logger::getInstance()->log(ss.str(), Logger::INFO2, name.str());

			resp = data->threadPool->cache.prepareResponse(req);
		}

		Logger::getInstance()->log("Responding...", Logger::INFO2, name.str());
		resp->respond(data->socketFd);
		Logger::getInstance()->log("Responded.", Logger::INFO2, name.str());

		delete resp;
		fclose(connection);
		Logger::getInstance()->log("Closed socket.", Logger::INFO2, name.str());

		if (data->socketFd == Thread::ENDING)
			break;

		pthread_mutex_lock(&data->mutex);
		data->socketFd = -1;
		Logger::getInstance()->log("Finished work", Logger::INFO2, name.str());
		pthread_mutex_unlock(&data->mutex);
	}

	Logger::getInstance()->log("Terminating", Logger::INFO1, name.str());
	return NULL;
}
