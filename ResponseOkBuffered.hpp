/**
 * @file ResponseOkBuffered.hpp
 * Contains definition of class ResponseOkBuffered.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEOKBUFFERED_HPP__
#define __RESPONSEOKBUFFERED_HPP__

#include "ResponseOk.hpp"

/**
 * A <i>200 OK</i> response with buffered content.
 * Content sent as part of the response is contained within a memory buffer.
 */
class ResponseOkBuffered : public ResponseOk
{
private:
	char *content; /**< Data content */
	unsigned long etag; /**< HTTP ETag of the sent file */
public:
	/**
	 * Creates an OK response with the given content.
	 * @param[in] content Content
	 * @param[in] contentLength Content's byte length
	 * @param[in] etag ETag of the file
	 * @param[in] contentType MIME type of the content
	 */
	ResponseOkBuffered(char *content, unsigned long contentLength, unsigned long etag = 0, const char *contentType = "");
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // RESPONSEOKBUFFERED_HPP
