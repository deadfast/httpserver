/**
 * @file ResponseBadRequest.cpp
 * Contains implementation of class ResponseBadRequest.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseBadRequest.hpp"

ResponseBadRequest::ResponseBadRequest() :
		Response("400 Bad Request")
{
}

bool ResponseBadRequest::writeHeader(FILE* stream) const
{
	return true;
}
bool ResponseBadRequest::writeContent(FILE* stream)
{
	return true;
}
