/**
 * @file Request.cpp
 * Contains implementation of class Request.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "Request.hpp"

#include <sstream>
#include <cstdio>

int Request::process(FILE* stream)
{
	std::string word;
	bool end;
	if (!getWord(stream, word, end, 8) || end)
		return INVALID;
	if (word == "POST" || word == "PUT" || word == "DELETE" ||
	        word == "TRACE" || word == "CONNECT" || word == "PATCH")
		return UNSUPPORTED;
	if (word == "GET")
		this->method = GET;
	else if (word == "HEAD")
		this->method = HEAD;
	else if (word == "OPTIONS")
		this->method = OPTIONS;
	else
		return INVALID;

	if (!getWord(stream, this->path, end, URI_LIMIT))
		return INVALID;
	if (!end) //Maintain HTTP/1.0 compatability
	{
		if (!getWord(stream, word, end, 10) || !end || word != "HTTP/1.1")
			return INVALID;
	}

	this->path = urlDecode(this->path);

	if (!pathValid(this->path)) //Protect against path traversal
		return INVALID;

	this->host = " ";
	this->etag = 0;
	std::string value;
	while (true)
	{
		getWord(stream, word, end, 50);
		if (end)
			break;
		getLine(stream, value, end, 500);
		if (word == "Host:")
		{
			this->host = value;
			trimHost();
		}
		else if (word == "If-None-Match:" && value.length() > 2)
		{
			std::stringstream(value.substr(1, value.length() - 2)) >> this->etag;
		}
		if (end)
			break;
	}

	return 0;
}
std::string Request::getRequestedPath() const
{
	return path;
}
std::string Request::getRequestedHost() const
{
	return host;
}
unsigned int Request::getETag() const
{
	return etag;
}
Request::Method Request::getMethod() const
{
	return method;
}
void Request::trimHost()
{
	for (unsigned int i = 0; i < host.size(); i++)
	{
		if (host[i] == ':')
		{
			host = host.substr(0, i);
			return;
		}
	}
}
bool Request::getWord(FILE* stream, std::string& word, bool& end, unsigned int limit)
{
	word = "";
	unsigned int count = 0;
	while (true)
	{
		int ch = fgetc(stream);
		if (ch == EOF)
		{
			word = "";
			end = false;
			return false;
		}

		count++;
		if (count != 0 && count > limit)
			return false;

		if (ch == ' ')
		{
			end = false;
			return true;
		}
		if (ch == '\r')
		{
			ch = fgetc(stream);
			if (ch == '\n')
			{
				end = true;
				return true;
			}
			else
			{
				word += '\r';
			}
		}
		word += ch;
	}
}
bool Request::getLine(FILE* stream, std::string& line, bool& end, unsigned int limit)
{
	line = "";
	unsigned int count = 0;
	while (true)
	{
		int ch = fgetc(stream);
		if (ch == EOF)
		{
			end = true;
			return true;
		}

		count++;
		if (count != 0 && count > limit)
			return false;

		if (ch == '\r')
		{
			ch = fgetc(stream);
			if (ch == '\n')
			{
				end = false;
				return true;
			}
			else
			{
				line += '\r';
			}
		}
		line += ch;
	}
}
bool Request::pathValid(const std::string& path)
{
	if (path.length() == 0)
		return true;
	unsigned int begin = (path[0] == '/') ? 1 : 0;
	for (unsigned int i = begin; i + 1 < path.size(); i += 3)
	{
		if (path[i] == '.' && path[i+1] == '.' && (i + 2 >= path.size() || path[i+2] == '/'))
			return false;
	}
	return true;
}
char Request::urlDecode(char d1, char d2)
{
	if (!((d1 >= 'A' && d1 <= 'F') || (d1 >= '0' && d1 <= '9')) ||
	        !((d2 >= 'A' && d2 <= 'F') || (d2 >= '0' && d2 <= '9')))
		return 0;

	return ((d1 >= 'A') ? (d1 - 'A' + 10) : (d1 - '0')) * 16 +
	       ((d2 >= 'A') ? (d2 - 'A' + 10) : (d2 - '0'));
}
std::string Request::urlDecode(const std::string& encoded)
{
	std::string decoded("");
	for (unsigned int i = 0; i < encoded.length(); i++)
	{
		if (encoded[i] == '%' && i + 2 < encoded.length())
		{
			char ch = urlDecode(encoded[i+1], encoded[i+2]);
			if (ch)
				decoded += ch;
			i += 2;
		}
		else
		{
			decoded += encoded[i];
		}
	}
	return decoded;
}
