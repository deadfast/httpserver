/**
 * @file ResponseNotModified.cpp
 * Contains implementation of class ResponseNotModified.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseNotModified.hpp"

ResponseNotModified::ResponseNotModified() :
		Response("304 Not Modified")
{
}

bool ResponseNotModified::writeHeader(FILE* stream) const
{
	return true;
}
bool ResponseNotModified::writeContent(FILE* stream)
{
	return true;
}
