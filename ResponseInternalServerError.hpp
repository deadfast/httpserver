/**
 * @file ResponseInternalServerError.hpp
 * Contains definition of class ResponseInternalServerError.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEINTERNALSERVERERROR_HPP__
#define __RESPONSEINTERNALSERVERERROR_HPP__

#include "Response.hpp"

/**
 * A <i>500 Internal Server Error</i> response.
 */
class ResponseInternalServerError : public Response
{
private:
	static const char *title; /**< Displayed title */
	char *reason; /**< Further reason */
public:
	/**
	 * Creates an internal server error response with a reason given as an ASCIIZ string.
	 * @param[in] reason Reason for the error
	 */
	ResponseInternalServerError(char *reason);
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEINTERNALSERVERERROR_HPP__
