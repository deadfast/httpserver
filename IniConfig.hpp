/**
 * @file IniConfig.hpp
 * Contains definition of class IniConfig and IniConfig::Section.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __INICONFIG_HPP__
#define __INICONFIG_HPP__

#include <string>
#include <map>

#include "exceptions.hpp"

/**
 * An INI file parser.
 *
 * A rudimentary parser for INI configuration files.
 * - Only letters from English alphabet allowed as property names
 * - No whitespace between property name and value allowed
 * - Blank lines allowed
 * - Sections supported, anything within the section marks [,] is considered its name, any characters allowed (except [ and ])
 * - Comment lines indicated by a semicolon as the first character on the line
 * - No duplicate names allowed
 */
class IniConfig
{
public:
	/**
	 * INI file section.
	 */
	class Section
	{
	private:
		std::map<std::string, std::string> entries; /**< Entries within the section */
	public:
		/**
		 * Creates an empty section.
		 */
		Section();

		/**
		 * Adds an entry to the section.
		 * Fails if an entry with the same name already exists within the section.
		 * @param[in] entry Entry
		 * @return Success
		 */
		bool addEntry(const std::pair<std::string, std::string> &entry);
		/**
		* Returns an entry with the given name.
		* @param[in] name Name
		* @return Entry
		* @throws UnknownIniSectionException Entry not found
		*/
		std::string& getEntry(const std::string& name) throw(UnknownIniEntryException);

		/**
		* Implements the << stream operator.
		* @param[out] os Output stream
		* @param[in] section Section to print
		* @return Output stream
		*/
		friend std::ostream& operator<<(std::ostream &os, const Section &section);
		/**
		* Prints the section into the stream
		* @param[out] os Stream
		*/
		void print(std::ostream &os) const;
	};

private:
	std::map<std::string, Section*> sections; /**< INI sections */
public:
	/**
	 * Creates an empty INI config.
	 */
	IniConfig();
	~IniConfig();

	/**
	 * Adds a section to the config.
	 * Fails if a section with the same name already exists.
	 * @param[in] section Section
	 * @return Success
	 */
	bool addSection(const std::pair<std::string, Section*> &section);
	/**
	 * Returns a pointer to the section with the given name.
	 * @param[in] name Name
	 * @return Pointer to section
	 * @throws UnknownIniSectionException Section not found
	 */
	Section* getSection(const std::string& name) throw(UnknownIniSectionException);

	/**
	 * Implements the << stream operator.
	 * @param[out] os Output stream
	 * @param[in] cfg IniConfig to print
	 * @return Output stream
	 */
	friend std::ostream& operator<<(std::ostream &os, const IniConfig &cfg);
	/**
	 * Prints the INI into the stream
	 * @param[out] os Stream
	 */
	void print(std::ostream &os) const;

	/**
	 * Parses the file for INI configuration data
	 * Fails if the file cannot be opened.
	 * Throws an exception with details in case the parsing fails.
	 * @param[in] fileName Name/path of the file
	 * @return Success
	 * @throws IniParserException Exception in parsing
	 */
	bool loadFromFile(const std::string fileName) throw(IniParserException);
	/**
	 * @copydoc IniConfig::loadFromFile
	 */
	bool loadFromFile(const char *fileName) throw(IniParserException);
	/**
	 * Writes contained configuration to the file in INI file format.
	 * If the file exists it will be overwritten.
	 * Fails if the file cannot be opened.
	 * @param[in] fileName Name/path of the file
	 * @return Success
	 */
	bool writeToFile(const std::string fileName);
	/**
	 * @copydoc IniConfig::writeToFile
	 */
	bool writeToFile(const char *fileName);

	/**
	 * Coverts the string to unsigned int
	 * If string cannot be converted value of result is not changed and the function returns false.
	 * @param[in] str String
	 * @param[out] result Destination for converted value
	 * @return Conversion successful
	 */
	static bool convertToUInt(const std::string &str, unsigned int &result);
};

#endif // __INICONFIG_HPP__
