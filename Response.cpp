/**
 * @file Response.cpp
 * Contains implementation of class Response.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "Response.hpp"

Response::Response(const char* responseName) :
		responseName(responseName)
{
}
Response::~Response()
{
}

bool Response::respond(int socketFd)
{
	FILE* stream = fdopen(socketFd, "w");
	if (!stream)
	{
		return false;
	}

	return (fputs("HTTP/1.1 ", stream) != EOF &&
	        writeResponseName(stream) &&
	        writeCommonHeader(stream) &&
	        writeHeader(stream) &&
	        fputs("\r\n\r\n", stream) != EOF &&
	        writeContent(stream) &&

	        fclose(stream) != EOF);
}
bool Response::writeCommonHeader(FILE* stream) const
{
	char date[33];
	writeDate(date);
	return (fprintf(stream, "\r\nDate: %s", date) != EOF &&
	        fputs("\r\nAccept-Ranges: none", stream) != EOF &&
	        fputs("\r\nConnection: close", stream) != EOF);
}
bool Response::writeResponseName(FILE* stream)
{
	return fputs(responseName, stream) != EOF;
}

void Response::writeDate(const time_t& dateTime, char* str)
{
	struct tm *timeInfo = gmtime(&dateTime);

	const char* wdays[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	const char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

	sprintf(str, "%s, %02d %s %d %02d:%02d:%02d GMT",
	        wdays[timeInfo->tm_wday],
	        timeInfo->tm_mday,
	        months[timeInfo->tm_mon],
	        timeInfo->tm_year + 1900,
	        timeInfo->tm_hour,
	        timeInfo->tm_min,
	        timeInfo->tm_sec
	       );
}
void Response::writeDate(char* str)
{
	time_t rawTime;
	time(&rawTime);
	writeDate(rawTime, str);
}
