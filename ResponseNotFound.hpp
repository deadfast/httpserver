/**
 * @file ResponseNotFound.hpp
 * Contains definition of class ResponseNotFound.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSENOTFOUND_HPP__
#define __RESPONSENOTFOUND_HPP__

#include "ResponseWithContent.hpp"

#include <cstdio>
#include <string>

/**
 * A <i>404 Not Found</i> response.
 */
class ResponseNotFound : public ResponseWithContent
{
private:
	std::string requestedAddress; /**< Address that was requested but not found */

	static const char* message; /**< Message to reply with to the client */
public:
	/**
	 * Creates a Not Found response.
	 * @param[in] requestedAddress Address that was not found
	 */
	ResponseNotFound(const std::string& requestedAddress);
protected:
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSENOTFOUND_HPP__
