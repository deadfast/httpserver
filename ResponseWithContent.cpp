/**
 * @file ResponseWithContent.cpp
 * Contains implementation of class ResponseWithContent.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseWithContent.hpp"

ResponseWithContent::ResponseWithContent(const char* responseName, long unsigned int contentLength, const char* contentType) :
		Response(responseName),
		contentLength(contentLength), contentType(contentType)
{
}

bool ResponseWithContent::writeHeader(FILE* stream) const
{
	return ((!*contentType ||
	         (*contentType && fprintf(stream, "\r\nContent-Type: %s", contentType) > 0)
	        ) &&
	        fprintf(stream, "\r\nContent-Length: %ld", contentLength) > 0);
}
