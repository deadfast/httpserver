/**
 * @file ResponseMethodNotAllowed.hpp
 * Contains definition of class ResponseMethodNotAllowed.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEMETHODNOTALLOWED_HPP__
#define __RESPONSEMETHODNOTALLOWED_HPP__

#include "Response.hpp"

/**
 * A <i>405 Method Not Allowed</i> response.
 */
class ResponseMethodNotAllowed : public Response
{
public:
	ResponseMethodNotAllowed();
protected:
	virtual bool writeHeader(FILE* stream) const;
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEMETHODNOTALLOWED_HPP__
