/**
 * @file ResponseWithContent.hpp
 * Contains definition of class ResponseWithContent.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEWITHCONTENT_HPP__
#define __RESPONSEWITHCONTENT_HPP__

#include "Response.hpp"

/**
 * An abstract base for a response that sends some content.
 * Only handles the response's head, content itself has to sent by the inheriting class.
 */
class ResponseWithContent : public Response
{
protected:
	unsigned long contentLength; /**< Lenght in bytes of the content */
	const char *contentType; /**< MIME type of the content */

	/**
	 * Assigns content head values.
	 * @param[in] responseName Name of the HTTP response
	 * @param[in] contentLength Length of the content
	 * @param[in] contentType MIME type of the content
	 */
	ResponseWithContent(const char* responseName, unsigned long contentLength, const char *contentType);

	virtual bool writeHeader(FILE* stream) const;
};

#endif // __RESPONSEWITHCONTENT_HPP__
