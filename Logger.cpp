/**
 * @file Logger.cpp
 * Contains implementation of class Logger.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "Logger.hpp"

#include <sstream>
#include <iomanip>
#include <ctime>

Logger *Logger::instance = new Logger();

Logger* Logger::getInstance()
{
	if (!instance->opened)
		return NULL;
	return instance;
}
void Logger::destroy()
{
	delete instance;
	instance = NULL;
}

Logger::Logger() :
		opened(false)
{
}
Logger::~Logger()
{
	if (opened)
	{
		file.close();
		if (mutexed)
			pthread_mutex_destroy(&mutex);
	}
}

bool Logger::openFile(const std::string& fileName, int verbosity, bool mutexed)
{
	return openFile(fileName.c_str(), verbosity, mutexed);
}
bool Logger::openFile(const char* fileName, int verbosity, bool mutexed)
{
	if (instance->opened)
		return false;
	if (mutexed)
		pthread_mutex_init(&instance->mutex, NULL);
	instance->verbosity = verbosity;
	instance->mutexed = mutexed;

	instance->file.open(fileName, std::ios::app);
	return (instance->opened = instance->file.is_open());
}

void Logger::log(const std::string& message, int severity, const std::string& source)
{
	if (mutexed)
		pthread_mutex_lock(&mutex);

	logToConsole(message, severity, source);
	logToFile(message, severity, source);

	if (mutexed)
		pthread_mutex_unlock(&mutex);
}
void Logger::logToFile(const std::string& message, int severity, const std::string& source)
{
	if (opened &&
	        (severity == Logger::ERROR || severity == Logger::WARNING ||
	         severity == Logger::INFO0 ||
	         (severity == Logger::INFO1 && verbosity >= 1) ||
	         (severity == Logger::INFO2 && verbosity >= 2) ||
	         verbosity >= 3)
	   )
	{
		writeCurrentTime(file);
		file << ((severity == Logger::ERROR) ? "[ERROR]" :
		         (severity == Logger::WARNING) ? "[WARNING]" : "[INFO]");
		if (source.length() > 0)
			file << "[" << source << "]";
		file << " " << message << std::endl;
	}
}
void Logger::logToConsole(const std::string& message, int severity, const std::string& source)
{
	if (severity == Logger::ERROR || verbosity > 0)
	{
		std::stringstream ss;
		if (source.length() > 0)
			ss << "[" << source << "] ";
		ss << message;

		if (severity == Logger::ERROR || (severity == Logger::WARNING && verbosity >= 1))
			std::cerr << ss.str() << std::endl;
		else if (verbosity >= 4)
			std::cout << ss.str() << std::endl;
	}
}

void Logger::writeCurrentTime(std::ostream& os)
{
	time_t rawTime;
	time(&rawTime);
	struct tm *timeInfo = localtime(&rawTime);
	os << "[" << (timeInfo->tm_year + 1900) << "-" << std::setw(2) << std::setfill('0') << (timeInfo->tm_mon + 1)
	<< "-" << std::setw(2) << std::setfill('0') << timeInfo->tm_mday
	<< "][" << std::setw(2) << std::setfill('0') << timeInfo->tm_hour
	<< ":" << std::setw(2) << std::setfill('0') << timeInfo->tm_min
	<< ":" << std::setw(2) << std::setfill('0') << timeInfo->tm_sec << "]";
}


