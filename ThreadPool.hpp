/**
 * @file ThreadPool.hpp
 * Contains definition of class ThreadPool and structure ThreadPool::Thread.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __THREADPOOL_HPP__
#define __THREADPOOL_HPP__

#include <sstream>
#include <pthread.h>
#include <cerrno>
#include <cstring>
#include <cstdio>

#include "Configuration.hpp"
#include "FileCache.hpp"

/**
 * An object responsible for handling the server's threads.
 * It is responsible for delegating work to individual threads as it comes in to facilitate parallel processing of requests.
 * In order to avoid overhead, started threads are re-used.
 */
class ThreadPool
{
private:
	/**
	 * A structure representing an individual server thread.
	 */
	struct Thread
	{
		static const int IDLE = -1; /**< Signalizes that the thread has no work. */
		static const int ENDING = -2; /**< Signals the thread it should terminate itself. */

		int id; /**< Thread ID */
		int socketFd; /**< Socket file descriptor or IDLE or ENDING. */
		pthread_mutex_t mutex; /**< Thread's mutex. */
		pthread_cond_t condition; /**< Thread's conditional variable. The thread waits for this to be signalled before processing any work. */
		pthread_t thread; /**< Thread's handle */
		ThreadPool *threadPool; /**< Pointer to thread pool */

		Thread();

		/**
		 * Starts the thread.
		 * @param[in] id Thread ID
		 * @param[in] threadPool Pointer to thread pool
		 * @return pthread_create return code
		 */
		int start(int id, ThreadPool *threadPool);
		/**
		 * Gracefully stops the thread.
		 */
		void stop();
	} *threads; /**< Array of worker threads */
	unsigned int maxThreads; /**< Number of worker threads */

	Configuration *config; /**< Pointer to Configuration */
	FileCache cache; /**< File cache */
public:
	/**
	 * Performs basic initialization of the thread pool.
	 * Prior to further use, ThreadPool::initialize() must first be called.
	 * @see ThreadPool::initialize()
	 * @param[in] maxThreads Max number of threads
	 * @param[in] config Pointer to Configuration
	 */
	ThreadPool(unsigned int maxThreads, Configuration *config);
	~ThreadPool();

	/**
	 * Finalizes the initialization process.
	 * Returns false in case of a critical error (a thread cannot be started). The application should be interrupted in that case.
	 * @return Success
	 */
	bool initialize();

	/**
	 * Find an idling thread that can process the incomming request and have it process it.
	 * In case there is no free thread available, it will reply with a 500 internal error message within the current thread.
	 * Returns false in case of a critical error (a thread cannot be started). The application should be interrupted in that case.
	 * @param[in] socketFd Socket descriptor
	 * @return Success
	 */
	bool delegateWork(int socketFd);
	
	/**
	 * Resets the thread with th given phread_t ID back to IDLE.
	 * @param[in] ptid pthread_t ID
	 */
	void resetThread(pthread_t ptid);

private:
	/**
	 * Starts a thread with the given ID.
	 * @param[in] id Thread ID
	 * @return Success
	 */
	bool startThread(unsigned int id);
	/**
	 * Attempts to find an indling thread.
	 * Returns the thread's index or -1 if there is no idle thread.
	 * @return Thread index
	 */
	int findIdle();
	/**
	 * Thread worker function.
	 * @param in Input parameter
	 */
	static void* workerThread(void *in);
};

#endif // __THREADPOOL_HPP__
