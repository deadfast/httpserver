/**
 * @file main.hpp
 * Contains definitions of the the main function and supporting functions.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>

#include "Configuration.hpp"
#include "ThreadPool.hpp"

#define EXIT_ERROR_PARAMETERS 1 /**< Exiting due to error in command-line parameters. */
#define EXIT_ERROR_INIPARSER 2 /**< Exiting due to error in INI parsing. */
#define EXIT_ERROR_CONFIG 3 /**< Exiting due to error while opening config file. */
#define EXIT_ERROR_BIND 4 /**< Exiting due to error in binding to a port. */
#define EXIT_ERROR_LISTEN 5 /**< Exiting due to error while listening for a connection. */
#define EXIT_ERROR_THREADS 6 /**< Exiting due to error while creating extra threads. */
#define EXIT_ERROR_LOG 7 /**< Exitting due to error opening the log file */

/**
 * Processes the command-line parameters.
 * @param[in] argc Number of parameters
 * @param[in] argv Array of parameters
 * @param[out] verbosity Level of verbosity
 * @param[out] mutexLog Mutexed logging
 * @return Program return code
 */
int processParameters(int argc, const char* argv[], int &verbosity, bool &mutexLog);

/**
 * Initializes the Configuration and Logger.
 * @param[in] configFile Path to config file
 * @param[in] verbosity Level of verbosity
 * @param[in] mutexLog Mutexed logging
 * @return Program return code
 */
int initializeConfigLogger(const char *configFile, int verbosity, bool mutexLog);

/**
 * Logs the event and terminates the server.
 * @param[in] returnCode Return code
 */
void terminate(int returnCode);

/**
 * Reports the message as error to the log and stderr.
 * @param[in] message
 */
void reportError(const std::string &message);

/**
 * Prints usage information about the program
 * @param[in] binName Name of the binary
 */
void printUsage(const char *binName);

/**
 * Hook for signal processing.
 * @param[in] signal Signal
 */
void signalHook(int signal);

/**
 * Returns a textual representation of the IP address.
 * The result's format (IPv6 vs. IPv4) depends on the type of the connection.
 */
void getIpAddress(sockaddr_storage *address, char result[INET6_ADDRSTRLEN]);

/**
 * Entry point of the application.
 * @param[in] argc Number of arguments
 * @param[in] argv Command-line arguments
 * @return Program return code
 */
int main(int argc, const char* argv[]);
