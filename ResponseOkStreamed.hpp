/**
 * @file ResponseOkStreamed.hpp
 * Contains definition of class ResponseOkStreamed.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEOKSTREAMED_HPP__
#define __RESPONSEOKSTREAMED_HPP__

#include "ResponseOk.hpp"

#include <fstream>

/**
 * A <i>200 OK</i> response with streamed content.
 * Content sent as part of the response is streamed directly from a file.
 */
class ResponseOkStreamed : public ResponseOk
{
private:
	std::ifstream fileStream; /**< File stream to the log file */
public:
	/**
	 * Creates an OK response where content will be streamed from the given file.
	 * @param[in] source Path to source file
	 * @param[in] contentLength Content's byte length
	 */
	ResponseOkStreamed(const std::string &source, unsigned long contentLength);
	virtual ~ResponseOkStreamed();
protected:
	virtual bool writeContent(FILE* stream);
};

#endif // __RESPONSEOKSTREAMED_HPP__
