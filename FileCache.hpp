/**
 * @file FileCache.hpp
 * Contains definition of class FileCache.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __FILECACHE_HPP__
#define __FILECACHE_HPP__

#include <map>
#include <sys/stat.h>

#include "Configuration.hpp"
#include "Response.hpp"
#include "Request.hpp"

/**
 * File cache.
 *
 * Handles file caching to allow for quick access.
 * Cached files age over time and unless refreshed, they will eventually be deleted.
 */
class FileCache
{
private:
	/**
	 * File cache entry.
	 */
	struct Entry
	{
		char *content; /**< Content of the file */
		unsigned long contentLen; /**< Lenght of content in bytes */
		unsigned long etag; /**< HTTP ETag identifier */
		const char *mime; /**< MIME type of the file */
		time_t expires; /**< Time of entry expiration */
		time_t mtime; /**< Last modified time of the file */

		~Entry();
	};
	std::map<std::string, Entry*> cache; /**< Cache map */
	unsigned long etagCounter; /**< Increasing counter for the ETag IDs */
	unsigned long currSize; /**< Current size of items contained within the cache in bytes */

	unsigned long maxCapacity; /**< Max capacity (memory usage) of the cache in bytes */
	unsigned long lifetime; /**< Lifetime of an entry */

	bool runCleaner; /**< Cleaner thread should be running */
	pthread_t cleaner; /**< Cleaner thread */
	pthread_mutex_t mutex; /**< Mutex for the cache */

	Configuration *config; /**< Pointer to Configuration */
public:
	/**
	 * Creates an empty cache. Further initialization has to be done via initialize().
	 * @param[in] config Pointer to config
	 * @see FileCache::initialize()
	 */
	FileCache(Configuration *config);
	/**
	 * Destroys the cache and frees all allocated memory.
	 */
	~FileCache();

	/**
	 * Finished initialization of the object.
	 * @return Success
	 */
	bool initialize();

	/**
	 * Prepares a Response to the received Request.
	 * @param[in] request client's Request
	 * @return Response to Request
	 */
	Response* prepareResponse(const Request &request);
private:
	/**
	 * Concatenates two paths together.
	 * @param[in] path1 First path
	 * @param[in] path2 Second path
	 * @return Concatenated path
	 */
	static std::string concatPath(const std::string& path1, const std::string& path2);
	/**
	 * Checks if the file exists.
	 * @param[in] path Path to the file
	 */
	static bool fileExists(const std::string& path);
	/**
	 * Searches the cache for the given file.
	 * @param[in] path Path to the file
	 * @return Pointer to cache entry or NULL if not found
	 */
	Entry* findInCache(const std::string &path) const;
	/**
	 * Creates a new cache entry and returns a pointer to it.
	 * Can fail in case there is not enough space in the cache for the entry, NULL is then returned.
	 * @param[in] path Path to file
	 * @param[in] info Information about the file
	 * @return Pointer to entry or NULL
	 */
	Entry* createCacheEntry(const std::string &path, const struct stat64 &info);

	/**
	 * Periodically scans the cache for expired entries and removes them.
	 * @param[in] in Pointer to cache
	 * @return Unused
	 */
	static void* cleanerThread(void *in);
};

#endif // __FILECACHE_HPP__

