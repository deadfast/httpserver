/**
 * @file MimeDetector.hpp
 * Contains definition of class MimeDetector.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __MIMEDETECTOR_HPP__
#define __MIMEDETECTOR_HPP__

#include <vector>
#include <iostream>
#include <fstream>

#include "exceptions.hpp"

/**
 * Detects MIME types of supplied files.
 * Implemented as a singletor, call getInstance() to retrieve the shared instance.
 * Call destroy() to perform cleanup prior to terminating the program.
 *
 * By default the following types are detected:
 * - image/gif
 * - image/jpeg
 * - image/png
 * - video/avi
 * - video/mp4
 * - video/ogg
 * - video/x-matroska
 * - video/x-matroska-3d
 * - audio/ogg
 * - audio/mp4
 * - audio/mpeg
 * - audio/x-matroska
 * - application/ogg
 * - application/pdf
 * - application/zip
 * - text/html
 * - text/css
 * - text/plain
 * @see MimeDetector::getInstance()
 * @see MimeDetector::destroy()
 */
class MimeDetector
{
private:
	static MimeDetector *instance; /**< Singleton instance */
	std::vector < const char*(*)(std::ifstream &, const std::string &) > testers; /**< Container for tester functions used to determine the MIME type */

public:
	/**
	 * Returns a shared instance of MimeDetector.
	 * @return Instance of MimeDetector
	 */
	static MimeDetector* getInstance();
	/**
	 * Performs cleanup.
	 */
	static void destroy();

	/**
	 * Registers a tester function.
	 * The function receives a reference to a file stream and extension of the file.
	 * It has to return either a MIME type in case the file is successfully recognized by the tester or an empty string if it is not.
	 * @param[in] function Tester function
	 */
	void registerTester(const char*(*function)(std::ifstream &, const std::string &));
	/**
	 * Attemps to identify the MIME type of the given file.
	 * Returns a MIME type or an empty string in case the type could not be reliably determined.
	 * @param[in] fileName Name/path of the file
	 * @throws FileIOException
	 * @return MIME type
	 */
	const char* detect(const std::string &fileName) const throw(FileIOException);
	/**
	 * Attemps to identify the MIME type of the given file.
	 * Returns a MIME type or an empty string in case the type could not be reliably determined.
	 * @param[in] fileName Name/path of the file
	 * @param[in] file File stream
	 * @return MIME type
	 */
	const char* detect(const std::string& fileName, std::ifstream& file) const;

private:
	/**
	 * Hidden for singleton
	 * @see MimeDetector::getInstance()
	 */
	MimeDetector();
	/**
	 * Hidden for singleton
	 * @see MimeDetector::getInstance()
	 */
	MimeDetector(MimeDetector const&) {};
	/**
	 * Hidden for singleton
	 * @see MimeDetector::getInstance()
	 */
	MimeDetector& operator=(MimeDetector const&) {return *this;}

	// ---------- MIME: image/??? ----------
	/**
	 * Detects GIF image.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectGif(std::ifstream &file, const std::string &extension);
	/**
	 * Detects JPEG image.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectJpeg(std::ifstream &file, const std::string &extension);
	/**
	 * Detects PNG image.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectPng(std::ifstream &file, const std::string &extension);
	/**
	 * Detects SVG image.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectSvg(std::ifstream &file, const std::string &extension);

	// ---------- MIME: video/??? ----------
	/**
	 * Detects AVI video.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectAvi(std::ifstream &file, const std::string &extension);
	/**
	 * Detects MP4 video.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMp4Video(std::ifstream &file, const std::string &extension);
	/**
	 * Detects Ogg video.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectOggVideo(std::ifstream &file, const std::string &extension);
	/**
	 * Detects Matroska video.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMatroska(std::ifstream &file, const std::string &extension);
	/**
	 * Detects 3D Matroska video.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMatroska3d(std::ifstream &file, const std::string &extension);

	// ---------- MIME: audio/??? ----------
	/**
	 * Detects Ogg audio.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectOggAudio(std::ifstream &file, const std::string &extension);
	/**
	 * Detects MP4 audio.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMp4Audio(std::ifstream &file, const std::string &extension);
	/**
	 * Detects MPEG audio (MP3).
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMpeg(std::ifstream &file, const std::string &extension);
	/**
	 * Detects Matroska audio.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectMatroskaAudio(std::ifstream &file, const std::string &extension);

	// ---------- MIME: application/??? ----------
	/**
	 * Detects Ogg multiplex.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectOggApp(std::ifstream &file, const std::string &extension);
	/**
	 * Detects PDF documents.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectPdf(std::ifstream &file, const std::string &extension);
	/**
	 * Detects ZIP archives.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectZip(std::ifstream &file, const std::string &extension);

	// ---------- MIME: text/??? ----------
	/**
	 * Detects HTML pages.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectHtml(std::ifstream &file, const std::string &extension);
	/**
	 * Detects CSS files.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 */
	static const char* detectCss(std::ifstream &file, const std::string &extension);
	/**
	 * Checks if the file is plaintext.
	 * NOTE: This is merely guesswork. A file is considered plaintext if there are no null values in the first 512 bytes.
	 * @param[in] file File stream
	 * @param[in] extension File extension
	 * @return MIME type
	 */
	static const char* detectPlaintext(std::ifstream &file, const std::string &extension);

	/**
	 * Checks if len bytes of the file match the bytes supplied
	 * @param[in] bytes Comparison
	 * @param[in] len Number of bytes
	 * @param[in] file File stream
	 * @return Match
	 */
	static bool checkBytes(unsigned char bytes[], int len, std::ifstream &file);
	/**
	 * Returns extension of the supplied file name
	 * @param[in] fileName File name
	 * @return File extension
	 */
	static std::string getExtension(const std::string &fileName);
};

#endif // __MIMEDETECTOR_HPP__
