/**
 * @file ResponseInternalServerError.cpp
 * Contains implementation of class ResponseInternalServerError.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseInternalServerError.hpp"
#include <cstring>

const char *ResponseInternalServerError::title = (char*) "<h1>500 Internal Server Error</h1>";

ResponseInternalServerError::ResponseInternalServerError(char* reason) :
		Response("500 Internal Server Error"),
		reason(reason)
{
}

bool ResponseInternalServerError::writeHeader(FILE* stream) const
{
	return (fputs("\r\nContent-Type: text/html", stream) != EOF &&
	        fprintf(stream, "\r\nContent-Length: %ld", (long) (12 + strlen(title) + strlen(reason) + 14)) > 0);
}
bool ResponseInternalServerError::writeContent(FILE* stream)
{
	return (fputs("<html><body>", stream) != EOF &&
	        fputs(title, stream) != EOF &&
	        fputs(reason, stream) != EOF &&
	        fputs("</body></html>", stream) != EOF);
}
