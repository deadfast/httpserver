/**
 * @file Configuration.hpp
 * Contains definition of class Configuration.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __CONFIGURATION_HPP__
#define __CONFIGURATION_HPP__

#include <string>
#include "IniConfig.hpp"

/**
 * Container for server configuration.
 *
 * Serves as a container for configuration loaded from an INI file using IniConfig object.
 */
class Configuration
{
private:
	std::string listenAddress; /**< Address to listen on for incomming connections */
	std::string listenPort; /**< Port to listen on for incomming connections */
	unsigned int cacheCapacity; /**< Max capacity of the cache */
	unsigned int cacheLifetime; /**< Lifetime of cache items */
	unsigned int maxConnections; /**< Max limit of simultaneous connections */
	std::string log; /**< Path to log file */
	std::string defaultServer; /**< Default virtual server */

	IniConfig *iniConfig; /**< Pointer to IniConfig */
public:
	/**
	 * Creates a new instance.
	 * NOTE: Values are not yet initialized at this point, you must call loadConfiguration() to initialize them manually.
	 * @see loadConfiguration()
	 */
	Configuration();
	~Configuration();
	/**
	 * Loads configuration from the ini config.
	 * Any errors during the loading will be printed to standard error.
	 * @param[in] ini Configuration source
	 * @return Success
	 */
	bool loadConfiguration(IniConfig* ini);

	/**
	 * Returns the address the server should listen on.
	 * @return Listen address
	 */
	std::string getListenAddress() const;
	/**
	 * Returns the port the server should listen on.
	 * @return Listen port
	 */
	std::string getListenPort() const;
	/**
	 * Return size of the file cache.
	 * @return File cache size
	 */
	unsigned int getCacheCapacity() const;
	/**
	 * Returns for how long files should be stored in the cache.
	 * @return Lifetime of cache items
	 */
	unsigned int getCacheLifetime() const;
	/**
	 * Returns the maximal number of simultaneous client connections.
	 * @return Max connections
	 */
	unsigned int getMaxConnections() const;
	/**
	 * Returns path to the log file.
	 * @return Path to log file
	 */
	std::string getLog() const;
	/**
	 * Returns name of the virtual server that should be used for legacy HTTP/1.0 connections that do not specify the destination host.
	 * @return Default virtual server name
	 */
	std::string getDefaultServer() const;

	/**
	 * Retrieves path to the virtual server's root.
	 * @param[in] name Virtual server name
	 * @param[out] path Path to server's root
	 * @returns 1 on success, 0 if server is not found, -1 if server is missing its root property
	 */
	int getServerPath(const std::string &name, std::string &path) const;
private:
	/**
	 * Prints error about expected unsigned integer to stderr.
	 * @param[in] name Option name
	 */
	void printUIntError(const char *name);
	/**
	 * Prints error about encountered zero.
	 * @param[in] name Option name
	 */
	void printZeroError(const char* name);
	/**
	 * Prints error about missing option to stderr.
	 * @param[in] name Option name
	 */
	void printMissingError(const char *name);
};

#endif // __CONFIGURATION_HPP__
