/**
 * @file MimeDetector.cpp
 * Contains implementation of class MimeDetector.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "MimeDetector.hpp"

MimeDetector* MimeDetector::instance = new MimeDetector();

MimeDetector* MimeDetector::getInstance()
{
	return instance;
}
void MimeDetector::destroy()
{
	delete instance;
	instance = NULL;
}

MimeDetector::MimeDetector()
{
	registerTester(MimeDetector::detectPlaintext);
	registerTester(MimeDetector::detectCss);
	registerTester(MimeDetector::detectHtml);
	registerTester(MimeDetector::detectAvi);
	registerTester(MimeDetector::detectMp4Video);
	registerTester(MimeDetector::detectOggVideo);
	registerTester(MimeDetector::detectMatroska);
	registerTester(MimeDetector::detectMatroska3d);
	registerTester(MimeDetector::detectOggAudio);
	registerTester(MimeDetector::detectMp4Audio);
	registerTester(MimeDetector::detectMpeg);
	registerTester(MimeDetector::detectMatroskaAudio);
	registerTester(MimeDetector::detectOggApp);
	registerTester(MimeDetector::detectPdf);
	registerTester(MimeDetector::detectZip);
	registerTester(MimeDetector::detectGif);
	registerTester(MimeDetector::detectJpeg);
	registerTester(MimeDetector::detectPng);
	registerTester(MimeDetector::detectSvg);
}
void MimeDetector::registerTester(const char*(*function)(std::ifstream &, const std::string &))
{
	testers.push_back(function);
}
const char* MimeDetector::detect(const std::string& fileName) const throw(FileIOException)
{
	std::string extension = MimeDetector::getExtension(fileName);
	std::ifstream file(fileName.c_str());
	if (!file.is_open())
		throw FileIOException(fileName, "could not open file");

	return detect(fileName, file);
}
const char* MimeDetector::detect(const std::string& fileName, std::ifstream& file) const
{
	std::string extension = MimeDetector::getExtension(fileName);

	for (std::vector < const char*(*)(std::ifstream &, const std::string &) >::const_iterator it = testers.end() - 1; it != testers.begin() - 1; --it)
	{
		file.clear();
		file.seekg(0, std::ios::beg);
		const char *mime = (*it)(file, extension);
		if (mime[0] != '\0')
			return mime;
	}

	return "";
}
const char* MimeDetector::detectGif(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x47, 0x49, 0x46, 0x38};
	if (extension == "gif" &&
	        checkBytes(bytes, 4, file)
	   )
		return "image/gif";
	else
		return "";
}
const char* MimeDetector::detectJpeg(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0xFF, 0xD8};
	if ((extension == "jpg" ||
	        extension == "jpeg" ||
	        extension == "jpe" ||
	        extension == "jif" ||
	        extension == "jfif" ||
	        extension == "jfi" ||
	        extension == "j") &&
	        checkBytes(bytes, 2, file)
	   )
		return "image/jpeg";
	else
		return "";
}
const char* MimeDetector::detectPng(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52};
	if (extension == "png" &&
	        checkBytes(bytes, 16, file)
	   )
		return "image/png";
	else
		return "";
}
const char* MimeDetector::detectSvg(std::ifstream& file, const std::string& extension)
{
	if (extension == "svg" ||
		extension == "svgz"
	)
		return "image/svg+xml";

	return "";
}
const char* MimeDetector::detectAvi(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x52, 0x49, 0x46, 0x46};
	if (extension == "avi" &&
	        checkBytes(bytes, 4, file)
	   )
		return "video/avi";
	else
		return "";
}
const char* MimeDetector::detectMp4Video(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x00, 0x00, 0x00};
	if ((extension == "mp4" ||
	        extension == "m4v" ||
	        extension == "mp4v") &&
	        checkBytes(bytes, 3, file)
	   )
		return "video/mp4";
	else
		return "";
}
const char* MimeDetector::detectOggVideo(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x4f, 0x67, 0x67, 0x53};
	if (extension == "ogv" &&
	        checkBytes(bytes, 4, file)
	   )
		return "video/ogg";
	else
		return "";
}
const char* MimeDetector::detectMatroska(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x1a, 0x45, 0xdf, 0xa3, 0x93, 0x42, 0x82, 0x88,
	                         0x6d, 0x61, 0x74, 0x72, 0x6f, 0x63, 0x6b, 0x61,
	                         0x42, 0x87, 0x81, 0x01, 0x42, 0x85, 0x81, 0x01,
	                         0x18, 0x53, 0x80, 0x67
	                        };
	if (extension == "mkv" &&
	        checkBytes(bytes, 28, file)
	   )
		return "video/x-matroska";
	else
		return "";
}
const char* MimeDetector::detectMatroska3d(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x1a, 0x45, 0xdf, 0xa3, 0x93, 0x42, 0x82, 0x88,
	                         0x6d, 0x61, 0x74, 0x72, 0x6f, 0x63, 0x6b, 0x61,
	                         0x42, 0x87, 0x81, 0x01, 0x42, 0x85, 0x81, 0x01,
	                         0x18, 0x53, 0x80, 0x67
	                        };
	if (extension == "mk3d" &&
	        checkBytes(bytes, 28, file)
	   )
		return "video/x-matroska-3d";
	else
		return "";
}
const char* MimeDetector::detectOggAudio(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x4f, 0x67, 0x67, 0x53};
	if ((extension == "ogg" ||
	        extension == "oga" ||
	        extension == "spx") &&
	        checkBytes(bytes, 4, file)
	   )
		return "audio/ogg";
	else
		return "";
}
const char* MimeDetector::detectMp4Audio(std::ifstream& file, const std::string& extension)
{
	if (extension == "m4a" ||
	        extension == "m4p" ||
	        extension == "m4b" ||
	        extension == "m4r"
	   )
		return "audio/mp4";
	else
		return "";
}
const char* MimeDetector::detectMpeg(std::ifstream& file, const std::string& extension)
{
	if (extension == "mp1" ||
	        extension == "mp2" ||
	        extension == "mp3"
	   )
		return "audio/mpeg";
	else
		return "";
}
const char* MimeDetector::detectMatroskaAudio(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x1a, 0x45, 0xdf, 0xa3, 0x93, 0x42, 0x82, 0x88,
	                         0x6d, 0x61, 0x74, 0x72, 0x6f, 0x63, 0x6b, 0x61,
	                         0x42, 0x87, 0x81, 0x01, 0x42, 0x85, 0x81, 0x01,
	                         0x18, 0x53, 0x80, 0x67
	                        };
	if (extension == "mka" &&
	        checkBytes(bytes, 28, file)
	   )
		return "audio/x-matroska";
	else
		return "";
}
const char* MimeDetector::detectOggApp(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x4f, 0x67, 0x67, 0x53};
	if (extension == "ogx" &&
	        checkBytes(bytes, 4, file)
	   )
		return "application/ogg";
	else
		return "";
}
const char* MimeDetector::detectPdf(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x25, 0x50, 0x44, 0x46, 0x2d};
	if (extension == "pdf" &&
	        checkBytes(bytes, 4, file)
	   )
		return "application/pdf";
	else
		return "";
}
const char* MimeDetector::detectZip(std::ifstream& file, const std::string& extension)
{
	unsigned char bytes[] = {0x50, 0x4b, 0x03, 0x04};
	if (extension == "zip" &&
	        checkBytes(bytes, 4, file)
	   )
		return "application/zip";
	else
		return "";
}
const char* MimeDetector::detectHtml(std::ifstream& file, const std::string& extension)
{
	if (detectPlaintext(file, extension)[0] == '\0') //Not even a text file
		return "";
	if (extension == "html" ||
	        extension == "htm"
	   )
		return "text/html";

	return "";
}
const char* MimeDetector::detectCss(std::ifstream& file, const std::string& extension)
{
	if (detectPlaintext(file, extension)[0] == '\0') //Not even a text file
		return "";
	if (extension == "css")
		return "text/css";

	return "";
}
const char* MimeDetector::detectPlaintext(std::ifstream& file, const std::string& extension)
{
	for (int i = 0; i < 512 && file.good(); i++)
	{
		if (file.get() == 0)
			return "";
	}
	return "text/plain";
}

std::string MimeDetector::getExtension(const std::string& fileName)
{
	for (int i = fileName.length() - 1; i >= 0; i--)
	{
		if (fileName[i] == '.')
		{
			std::string ext = fileName.substr(i + 1);
			for (unsigned int j = 0; j < ext.length(); j++)
				ext[j] = tolower(ext[j]);

			return ext;
		}
		if (fileName[i] == '/')
			break;
	}

	return std::string("");
}
bool MimeDetector::checkBytes(unsigned char bytes[], int len, std::ifstream& file)
{
	for (int i = 0; i < len; i++)
	{
		if (!file.good() || file.get() != bytes[i])
			return false;
	}
	return true;
}
