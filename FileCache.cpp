/**
 * @file FileCache.cpp
 * Contains implementation of class FileCache.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "FileCache.hpp"

#include <cerrno>
#include <ctime>
#include <sstream>
#include <cstdio>
#include <cstring>
#include <unistd.h>

#include "Logger.hpp"
#include "MimeDetector.hpp"
#include "ResponseOptions.hpp"
#include "ResponseInternalServerError.hpp"
#include "ResponseNotFound.hpp"
#include "ResponseOkBuffered.hpp"
#include "ResponseOkStreamed.hpp"
#include "ResponseNotModified.hpp"
#include "ResponseMovedPermanently.hpp"

FileCache::Entry::~Entry()
{
	delete[] content;
}

FileCache::FileCache(Configuration* config) :
		etagCounter(time(NULL)),
		currSize(0),
		maxCapacity(config->getCacheCapacity() * 1024),
		lifetime(config->getCacheLifetime()),
		config(config)
{
}
FileCache::~FileCache()
{
	if (maxCapacity > 0)
	{
		runCleaner = false;
		pthread_mutex_destroy(&mutex);
		Logger::getInstance()->log("Terminating cleaner thread...", Logger::INFO1, "FileCache::~FileCache");
		pthread_join(cleaner, NULL);
		Logger::getInstance()->log("Cleaner thread terminated.", Logger::INFO1, "FileCache::~FileCache");
		for (std::map<std::string, Entry*>::iterator it = cache.begin(); it != cache.end(); ++it)
		{
			delete it->second;
		}
	}
}
bool FileCache::initialize()
{
	pthread_mutex_init(&mutex, NULL);

	if (maxCapacity > 0)
	{
		runCleaner = true;
		int rc = pthread_create(&cleaner, NULL, FileCache::cleanerThread, (void*) this);
		if (rc)
		{
			std::ostringstream ss;
			ss << "Failed to start cleaner thread, pthread_create returned " << rc << ": " << strerror(errno);
			Logger::getInstance()->log(ss.str(), Logger::ERROR, "FileCache::initialize");
			return false;
		}
	}
	return true;
}

Response* FileCache::prepareResponse(const Request& request)
{
	if (request.getMethod() == Request::OPTIONS)
		return new ResponseOptions();

	std::string fullPath;
	int result = config->getServerPath(request.getRequestedHost(), fullPath);
	if (result == -1) //root missing in config
	{
		std::ostringstream msg;
		msg << "Missing \'root\' entry for server \'" << request.getRequestedHost() << "\'";
		Logger::getInstance()->log(msg.str(), Logger::ERROR);

		return new ResponseInternalServerError((char*) "Improper configuration");
	}
	if (result == 0) //Virtual server not found, use default
	{
		config->getServerPath(config->getDefaultServer(), fullPath);
	}

	fullPath += request.getRequestedPath();

	struct stat64 info;
	memset(&info, 0, sizeof(info));
	if (stat64(fullPath.c_str(), &info) != 0 || (!S_ISREG(info.st_mode) && !S_ISLNK(info.st_mode)))
	{
		if (S_ISDIR(info.st_mode))
		{
			if (fileExists(concatPath(fullPath, "index.html")))
				return new ResponseMovedPermanently(concatPath(request.getRequestedPath(), "index.html"));
		}
		return new ResponseNotFound(request.getRequestedPath());
	}

	pthread_mutex_lock(&mutex);
	Entry *entry = findInCache(fullPath);
	if (entry && entry->mtime < info.st_mtime) //Entry exists but is outdated
	{
		delete entry;
		entry = NULL;
	}
	if (entry)
	{
		entry->expires = time(NULL) + (lifetime * 60); //Refresh expiration
		pthread_mutex_unlock(&mutex);
	}
	else
	{
		entry = createCacheEntry(fullPath, info);
		pthread_mutex_unlock(&mutex);
		if (!entry) //Couldn't create entry - cache full
		{
			std::ostringstream msg;
			msg << "Cannot cache " << request.getRequestedPath() << ", streaming instead";
			Logger::getInstance()->log(msg.str(), Logger::INFO2, "FileCache::prepareResponse");
			if (request.getMethod() == Request::HEAD)
				return new ResponseOkBuffered(NULL, info.st_size, 0, MimeDetector::getInstance()->detect(fullPath));
			else
				return new ResponseOkStreamed(fullPath, info.st_size);
		}
	}
	if (request.getMethod() == Request::HEAD) //Only wants the response head, no content
		return new ResponseOkBuffered(NULL, entry->contentLen, entry->etag, entry->mime);
	
	if (request.getETag() == entry->etag)
		return new ResponseNotModified();
	else
		return new ResponseOkBuffered(entry->content, entry->contentLen, entry->etag, entry->mime);
}

std::string FileCache::concatPath(const std::string& path1, const std::string& path2)
{
	return path1 + ((path1[path1.length()-1] != '/' && path2[0] != '/') ? "/" : "") + path2;
}

bool FileCache::fileExists(const std::string& path)
{
	struct stat info;
	memset(&info, 0, sizeof(info));
	return (stat(path.c_str(), &info) == 0 &&
	        (S_ISREG(info.st_mode) || S_ISLNK(info.st_mode)));
}

FileCache::Entry* FileCache::findInCache(const std::string& path) const
{
	std::map<std::string, Entry*>::const_iterator it = cache.find(path);
	if (it == cache.end())
		return NULL;
	else
		return it->second;
}
FileCache::Entry* FileCache::createCacheEntry(const std::string& path, const struct stat64 &info)
{
	if (info.st_size + currSize > maxCapacity) //File too big to fit into cache
		return NULL;

	std::ifstream file(path.c_str(), std::ios::binary);
	if (!file.is_open())
	{
		std::cout << "not open";
		return NULL;
	}

	Entry *entry = new Entry;
	entry->content = new char [entry->contentLen = info.st_size];
	file.read(entry->content, entry->contentLen);
	entry->etag = ++etagCounter;
	entry->mime = MimeDetector::getInstance()->detect(path, file);
	entry->expires = time(NULL) + (lifetime * 60);
	entry->mtime = info.st_mtime;

	file.close();

	cache[path] = entry;

	currSize += entry->contentLen;

	std::ostringstream ss;
	ss << "Cached new item: " << path;
	Logger::getInstance()->log(ss.str(), Logger::INFO2, "FileCache::createCacheEntry");

	return entry;
}

void* FileCache::cleanerThread(void* in)
{
	FileCache *cache = (FileCache*) in;
	Logger::getInstance()->log("Cleaner thread started.", Logger::INFO1, "FileCache::cleanerThread");

	while (true)
	{
		for (unsigned int i = cache->lifetime * 60; i > 0; i--)
		{
			sleep(1);
			if (!cache->runCleaner)
				return NULL;
		}

		pthread_mutex_lock(&cache->mutex);
		Logger::getInstance()->log("Performing cleanup", Logger::INFO1, "FileCache::cleanerThread");

		std::map<std::string, Entry*>::iterator it = cache->cache.begin(), prev;
		while (it != cache->cache.end())
		{
			prev = it;
			++it;
			if (prev->second->expires < time(NULL))
			{
				std::ostringstream msg;
				msg << "Removing expired item: " << prev->first;
				Logger::getInstance()->log(msg.str(), Logger::INFO2, "FileCache::cleanerThread");
				cache->currSize -= prev->second->contentLen;
				delete prev->second;
				cache->cache.erase(prev);
			}
		}
		pthread_mutex_unlock(&cache->mutex);
	}
}
