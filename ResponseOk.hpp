/**
 * @file ResponseOk.hpp
 * Contains definition of class ResponseOk.
 *
 * @author Petr Klima
 * @version 0.1
 */

#ifndef __RESPONSEOK_HPP__
#define __RESPONSEOK_HPP__

#include "ResponseWithContent.hpp"

/**
 * An abstract base for a <i>200 OK</i> response.
 */
class ResponseOk : public ResponseWithContent
{
protected:
	/**
	 * Creates an OK response.
	 * @param[in] contentLength Length of the response
	 * @param[in] contentType MIME type of the content
	 */
	ResponseOk(unsigned long contentLength, const char *contentType);
};

#endif // __RESPONSEOK_HPP__
