/**
 * @file ResponseNotFound.cpp
 * Contains implementation of class ResponseNotFound.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseNotFound.hpp"

#include <cstring>

const char* ResponseNotFound::message = "<html><head><title>404 - Not Found</title></head><body><h1>404 - Not Found</h1>The requested URL %s was not found on this server.";

ResponseNotFound::ResponseNotFound(const std::string& requestedAddress)
		: ResponseWithContent("404 Not Found",
		                      strlen(message) - 2 + requestedAddress.length(),
		                      "text/html")
{
	this->requestedAddress = requestedAddress;
}

bool ResponseNotFound::writeContent(FILE* stream)
{
	return fprintf(stream, message, requestedAddress.c_str()) != EOF;
}
