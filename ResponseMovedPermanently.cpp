/**
 * @file ResponseMovedPermanently.cpp
 * Contains implementation of class ResponseMovedPermanently.
 *
 * @author Petr Klima
 * @version 0.1
 */

#include "ResponseMovedPermanently.hpp"

#include <cstring>

const char* ResponseMovedPermanently::text = "<a href=\"%s\">%s</a>";

ResponseMovedPermanently::ResponseMovedPermanently(const std::string& movedTo) :
		ResponseWithContent("301 Moved Permanently", strlen(text) + (2 * (movedTo.length() - 2)), "text/html"),
		movedTo(movedTo)
{
}

bool ResponseMovedPermanently::writeHeader(FILE* stream) const
{
	return (ResponseWithContent::writeHeader(stream) &&
	        fprintf(stream, "\r\nLocation: %s", movedTo.c_str()) != EOF);
}
bool ResponseMovedPermanently::writeContent(FILE* stream)
{
	return fprintf(stream, text, movedTo.c_str(), movedTo.c_str()) != EOF;
}
